﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="property.aspx.cs" Inherits="property" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--Page Title-->
<title>Mohan Estates Developers</title>
<!--Meta Tags-->
<meta charset="UTF-8"/>
<meta name="author" content=""/>
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<!-- Set Viewport-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css"/>
<link rel="stylesheet" href="css/font-awesome.min.css"/>
<link rel="stylesheet" href="css/flexslider.css"/>
<link rel="stylesheet" href="css/select-theme-default.css"/>
<link rel="stylesheet" href="css/owl.carousel.css"/>
<link rel="stylesheet" href="css/owl.theme.css"/>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
    <form id="form1" runat="server">
   <header>
<div id="top-strip">
	<div class="container">
		<ul class="pull-left social-icons">
			<li><a href="#" class="fa fa-google-plus"></a></li>
			<li><a href="#" class="fa fa-twitter"></a></li>
			<li><a href="#" class="fa fa-pinterest"></a></li>
			<li><a href="#" class="fa fa-dribbble"></a></li>
			<li><a href="#" class="fa fa-linkedin"></a></li>
			<li><a href="#" class="fa fa-facebook"></a></li>
		</ul>
        <div id="login-box" class='pull-right'>
            <a class='fa fa-phone'><span>+91-9818060989 | +91-9999654898</span></a>

        </div>
	</div>
</div>
</header>
<!-- /Header -->
<div class="slider-section">
	<div id="premium-bar">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
                    <a class="navbar-brand" href="Default.aspx"><img src="img/logo.jpg" alt="logo"/></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="Default.aspx">Home</a></li>
                        <li><a href="About.aspx">About Us</a></li>
                        <li class="active"><a href="listing.aspx">Properties</a></li>
                        <li><a href="interiors.aspx">Interiors</a></li>
                        
                        <li><a href="Contactus.aspx">Contact Us</a></li>

                    </ul>
                </div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
			</nav>
		</div>
	</div>
	<!-- head-Section -->
	<div class="page-title-section">
		<div class="container">
            
			<div class="pull-left page-title">
				<a href="#">
				<h2>property listings</h2>
				</a>
			</div>
			<!--<div class="pull-right breadcrumb">
				<a href="#">home</a><span class="fa fa-arrow-circle-right sep"></span><a href="#">property listings</a>
			</div>-->
		</div>
	</div>
</div>
    <!-- Search-Section -->
    <div class="search-section">
        <div class="container">
           <div class="container">
		
			<div class="select-wrapper select-big" id='select-rent'>
				<p>
					 Looking For
				</p>
                <asp:DropDownList ID="PropertyTypeDropdown" runat="server" class='elselect'>
                    <asp:ListItem>Rent</asp:ListItem>
                    <asp:ListItem>Sale</asp:ListItem>
                   
                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big" id='select-property'>
				<p>
					 Property type
				</p>
                <asp:DropDownList ID="PurposeDropdown" runat="server" class='elselect'>
                    <asp:ListItem>Residential</asp:ListItem>
                    <asp:ListItem>Commercials</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big">
				<p>
					 Location
				</p>
                  <asp:DropDownList ID="LocationDropdown" runat="server" class='elselect'>
                    <asp:ListItem>All</asp:ListItem>
                       <asp:ListItem>Pitampura</asp:ListItem>
                                            <asp:ListItem>Rohini</asp:ListItem>
                                            <asp:ListItem>Netaji Subhas Place</asp:ListItem>
                                            <asp:ListItem>Paschim Vihar</asp:ListItem>
                                           
                                            <asp:ListItem>Yamuna Expressway</asp:ListItem>
                                            <asp:ListItem>Noida</asp:ListItem>
                                            <asp:ListItem>Gurgaon</asp:ListItem>
                    
                </asp:DropDownList>
				
			</div>
			
			
			
			<div class="select-wrapper select-big">
				<p>
					 Min Price
				</p>
                 <asp:DropDownList ID="MinPriceDropdown" runat="server" class='elselect'>
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="10000">₹ 10000</asp:ListItem>
                      <asp:ListItem Value="25000">₹ 25000</asp:ListItem>
                      <asp:ListItem Value="50000">₹ 50000</asp:ListItem>
                      <asp:ListItem Value="100000">₹ 100000</asp:ListItem>
                      <asp:ListItem Value="125000">₹ 125000</asp:ListItem>
                      <asp:ListItem Value="150000">₹ 150000</asp:ListItem>

                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big">
				<p>
					 Max Price
				</p>
				<asp:DropDownList ID="MaxPriceDropdown" runat="server" class='elselect'>
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="100000">₹ 100000</asp:ListItem>
                      <asp:ListItem Value="250000">₹ 250000</asp:ListItem>
                      <asp:ListItem Value="500000">₹ 500000</asp:ListItem>
                      <asp:ListItem Value="1000000">₹ 1000000</asp:ListItem>
                      <asp:ListItem Value="1250000">₹ 1250000</asp:ListItem>
                      <asp:ListItem Value="1500000">₹ 1500000</asp:ListItem>

                </asp:DropDownList>
			</div>
            <asp:Button ID="SearchButton" runat="server" Text="Search" class='yellow-btn' OnClick="SearchButton_Click" />
			
	
	</div>
        </div>
    </div>
<!-- content-Section -->
<div class="content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 page-content">
				<div class="inner-wrapper">
					<div class="property-images-slider">
						<div id="details-slider" class="flexslider">
							<a href="#" class='fa fa-home property-type-icon'></a>
							<a href="#" class="yellow-btn">₹ <asp:Label ID="LabelPrice" runat="server" Text="Label"></asp:Label></a>
							<a href="#" class='status'>for <asp:Label ID="LabelType" runat="server" Text="Label"></asp:Label></a>
							<ul class="slides">
								<li>
								<div class="image-wrapper">
                                    <asp:Image ID="Image1" runat="server" />
									
								</div>
								</li>
								<li>
								<div class="image-wrapper">
                                     <asp:Image ID="Image2" runat="server" />
								</div>
								</li>
								<li>
								<div class="image-wrapper">
									  <asp:Image ID="Image3" runat="server" />
								</div>
								</li>
								<li>
								<div class="image-wrapper">
                                     <asp:Image ID="Image4" runat="server" />
								</div>
								</li>
								<li>
								<div class="image-wrapper">
                                     <asp:Image ID="Image5" runat="server" />
								</div>
								</li>
								
								
							</ul>
						</div>
						<div id="details-carousel" class="flexslider">
							<ul class="slides">
								<li>
                                     <asp:Image ID="Image6" runat="server" />
								</li>
								<li>
                                     <asp:Image ID="Image7" runat="server" />
								</li>
								<li>
                                     <asp:Image ID="Image8" runat="server" />
								</li>
								<li>
                                     <asp:Image ID="Image9" runat="server" />
								</li>
								<li>
								  <asp:Image ID="Image10" runat="server" />
								</li>
								
							</ul>
						</div>
					</div>
					<div class="property-desc">
						<h3>
                                        <asp:Label ID="LabelAddress" runat="server" Text="Label"></asp:Label></h3>
						<ul class="slide-item-features item-features">
							<li><span class="fa fa-arrows-alt"></span>
                                            <asp:Label ID="LabelArea" runat="server" Text="Label"> sq ft</asp:Label></li>
							
						</ul>
						<div class="first-paragraph">
							<span id="labeldescription" runat="server"></span>
						</div>
						
						<div class="additional-features">
							<h3>Additional Features</h3>
							<ul class="features" id="featuresPage" runat="server">
								
							</ul>
						</div>
						<div class="property-location">
							<h3>Property Location</h3>
							<div id="property-location-map">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 blog-sidebar">
				<div class="sidebar-widget author-profile">
					
					<div class="desc-box">
						<h4>
                             <asp:Label ID="LabelTitle" runat="server" Text="Label"></asp:Label></h4>
						<p class="person-number">
							<i class="fa fa-phone"></i> 900 123 456 789
						</p>
						<p class="person-email">
							<i class="fa fa-envelope"></i> welcome@dmall.com
						</p>
						<p class="person-fax">
							<i class="fa fa-print"></i> 900 123 456 789
						</p>
						<a href="#" class='gray-btn'>Contact Us</a>
					</div>
				</div>
				<div class="sidebar-widget similar-listings-widget">
					<h4 class="widget-title">similar listings</h4>
					<ul class="similar-listings">
						<li class="tab-content-item">
						<div class="pull-left thumb">
							<img src="img/blog/25_blog-thumb_1.png" alt="thumbnail">
						</div>
						<h5><a href="#">Netaji Subhas Place - ₹ 75,000</a></h5>
						</li>
						<li class="tab-content-item">
						<div class="pull-left thumb">
							<img src="img/blog/26_blog-thumb_2.png" alt="thumbnail">
						</div>
						<h5><a href="#">Paschim Vihar - ₹ 70,000</a></h5>
						</li>
						<li class="tab-content-item">
						<div class="pull-left thumb">
							<img src="img/blog/27_blog-thumb_3.png" alt="thumbnail">
						</div>
                            <h5><a href="#">Paschim Vihar - ₹ 70,000</a></h5>
						</li>
					</ul>
				</div>
				<div class="sidebar-widget text-widget">
					<h4 class="widget-title">Text Widget</h4>
					<p class='first-paragraph'>
						This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum
					</p>
					<p>
						 auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
    <!-- footer-section -->
    <footer>
        <div class="container">
            <div class="col-md-3 footer-about">
                <a class="logo" href="#"><img src="img/footer-logo.jpg" alt="logo"/></a>
                <p>
                    Lorem ipsum dolor sit amet consectetur incididunt ut labore et dolore magna aliqua adipisicing elit seddo eiusmod tempor.
                </p>
            </div>
            <div class="col-md-3 footer-recent-posts">
                <h3 class='footer-title'>ADDRESS</h3>
                <ul>
                    <li>
                        <a href="#">
                            <i class="fa fa-map-marker"></i>  Ground Floor-6, Dmall,
                            Netaji subhash palace, New Delhi
                        </a>
                    </li>
                    <li><a href="#"><i class="fa fa-phone"></i>+91-98 1806 0989</a></li>
                    <li><a href="#"><i class="fa fa-globe"></i>mohanassociates.com</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>contact@mohanassociates.com</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-contact-info">
                <h3 class='footer-title'>WE ARE SOCIAL</h3>
                <p class="website-number">
                    <i class="fa fa-facebook"></i> Facebook
                </p>
                <p class="website-email">
                    <i class="fa fa-twitter"></i> Twitter
                </p>
                <p class="website-fax">
                    <i class="fa fa-google-plus"></i>Google Plus
                </p>
            </div>
            <div class="col-md-3 footer-recent-posts">
                <h3 class='footer-title'>Other Links</h3>
                <ul>
                    <li>
                        <a>About Us</a>
                    </li>
                    <li>
                        <a>Contact Us</a>
                    </li>

                    <li>
                        <a>Career</a>
                    </li>
                    <li>
                        <a>Terms and Conditions</a>
                    </li>
                    <li>
                        <a>Privacy Policies</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
<div class="bottom-strip">
	<div class="container">
		<div class="col-md-4">
            <p class='pull-left'>
                © 2017 Mohan Estates Developers, All Rights Reserved
            </p>
		</div>
		<div class="col-md-4" class='bottom-strip-middle'>
			<a href="#top" class='fa fa-arrow-circle-up' id='backtop-btn'></a>
		</div>
		<div class="col-md-4">
			<ul class="social-icons">
				<li><a href="#" class="fa fa-google-plus"></a></li>
				<li><a href="#" class="fa fa-twitter"></a></li>
				<li><a href="#" class="fa fa-pinterest"></a></li>
				<li><a href="#" class="fa fa-dribbble"></a></li>
				<li><a href="#" class="fa fa-linkedin"></a></li>
				<li><a href="#" class="fa fa-facebook"></a></li>
			</ul>
		</div>
	</div>
</div>
<!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/select.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAR1B1lSLpfOjpUsnDMS9c6FTo3eQJSTUs&sensor=false"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // Google Map
        var map;
        var marker;
        var MyMarker;
        function initialize() {
            var map_canvas = document.getElementById('property-location-map');
            var myLatlng = new google.maps.LatLng(28.6928764, 77.1526366);
            var mapOptions = {
                center: myLatlng,
                zoom: 17,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(map_canvas, mapOptions);
            TestMarker();
        }
        // Function for adding a marker to the page.
        function addMarker(location) {
            marker = new google.maps.Marker({
                position: location,
                icon: 'img/property/50_property-marker.png',
                map: map
            });
        }
        // Testing the addMarker function
        function TestMarker() {
            MyMarker = new google.maps.LatLng(28.6928764, 77.1526366);
            addMarker(MyMarker);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    });
	</script>
    </form>
</body>
</html>
