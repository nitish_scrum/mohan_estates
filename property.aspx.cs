﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class property : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string propertyId = (String)Request.QueryString["prop"];
            LoadDetails(propertyId);
        }
    }

    protected void LoadDetails(string propertyId)
    {
        Property property = GetPropertyDetailsById(Int32.Parse(propertyId));
        LabelPrice.Text = property.Price.ToString();
        LabelAddress.Text = property.Address;
        LabelArea.Text = property.CoveredArea.ToString();
        labeldescription.InnerHtml = property.Description;
        LabelTitle.Text = property.Title;
        LabelType.Text = property.PropertyType;


        Image1.ImageUrl = "images/property/" + property.Image1;
        Image6.ImageUrl = "images/property/" + property.Image1;
        if (property.Image2 != null && !property.Image2.Trim().Equals(""))
        {
            Image2.ImageUrl = "images/property/" + property.Image2;
            Image7.ImageUrl = "images/property/" + property.Image2;

        }
        if (property.Image3 != null && !property.Image3.Trim().Equals(""))
        {
            Image3.ImageUrl = "images/property/" + property.Image3;
            Image8.ImageUrl = "images/property/" + property.Image3;
        }

        if (property.Image4 != null && !property.Image4.Trim().Equals(""))
        {
            Image4.ImageUrl = "images/property/" + property.Image4;
            Image9.ImageUrl = "images/property/" + property.Image4;
        }

        if (property.Image5 != null && !property.Image5.Trim().Equals(""))
        {
            Image5.ImageUrl = "images/property/" + property.Image5;
            Image10.ImageUrl = "images/property/" + property.Image5;
        }

     
        string features = property.Features;
        string[] featuresContent = features.Split(',');
        string featuresTxt = "";
        for (int i = 0; i < featuresContent.Length; i++)
        {
            featuresTxt += "<li class='single-feature'><a href='#'><i class='fa fa-check-circle'></i>" + featuresContent[i] + "</a></li>";
        }
        featuresPage.InnerHtml = featuresTxt;
    //    detailsTxt += "</ul></div></section>";
//

     //   detailsTxt += "<section id = 'map-section' ><div class='section-content'><div class='section-header onscroll-animate' data-animation='fadeInLeft'>" +
       //               "<h1>Rezidence Location</h1></div>";
       
 //       detailsTxt += "<iframe width = '100%' height = '450' frameborder = '0' style = 'border:0' src = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyBfKL-tddgOEY8XzrKVu-haJHS3s4EnKTU &q=" + property.Address.Replace(" ", "+") + "' allowfullscreen >" +
  //     "</iframe>";
   //     detailsTxt += "</div></section>";
//

  //      detailsTxt += "</div></div>";
  //      PropertyDetailsContainer.InnerHtml = detailsTxt;
    }

    protected Property GetPropertyDetailsById(int propertyId)
    {
        Property property = null;
        AdminDao dao = new AdminDao();
        property = dao.GetPropertyDetailsById(propertyId);
        return property;
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        string purpose = PurposeDropdown.SelectedValue;
        string location = LocationDropdown.SelectedValue;
        string propertyType = PropertyTypeDropdown.SelectedValue;

        string minPrice = MinPriceDropdown.SelectedValue;
        string maxPrice = MaxPriceDropdown.SelectedValue;

        String query = "select * from property where property_type = '" + propertyType + "'";

        if (!purpose.Equals("All"))
        {
            query += " and purpose = '" + purpose + "'";
        }
        if (!location.Equals("All"))
        {
            query += " and location = '" + location + "'";
        }

        if (!minPrice.Equals("0"))
        {
            query += " and price > " + minPrice;
        }
        if (!maxPrice.Equals("0"))
        {
            query += " and price < " + maxPrice;
        }

        Session["SearchQuery"] = query;
        Response.Redirect("listing.aspx");
    }
}