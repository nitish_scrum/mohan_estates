﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Contactus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        string purpose = PurposeDropdown.SelectedValue;
        string location = LocationDropdown.SelectedValue;
        string propertyType = PropertyTypeDropdown.SelectedValue;

        string minPrice = MinPriceDropdown.SelectedValue;
        string maxPrice = MaxPriceDropdown.SelectedValue;

        String query = "select * from property where property_type = '" + propertyType + "'";

        if (!purpose.Equals("All"))
        {
            query += " and purpose = '" + purpose + "'";
        }
        if (!location.Equals("All"))
        {
            query += " and location = '" + location + "'";
        }

        if (!minPrice.Equals("0"))
        {
            query += " and price > " + minPrice;
        }
        if (!maxPrice.Equals("0"))
        {
            query += " and price < " + maxPrice;
        }

        Session["SearchQuery"] = query;
        Response.Redirect("listing.aspx");
    }
    protected void SendMessage_Click(object sender, EventArgs e)
    {

    }
}