﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PostRequirement.aspx.cs" Inherits="PostRequirement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <!--Page Title-->
    <title>Mohan Estates Developers</title>
    <!--Meta Tags-->
<meta charset="UTF-8"/>
<meta name="author" content=""/>
<meta name="keywords" content=""/>
<meta name="description" content=""/>
    <!-- Set Viewport-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css"/>
<link rel="stylesheet" href="css/font-awesome.min.css"/>
<link rel="stylesheet" href="css/flexslider.css"/>
<link rel="stylesheet" href="css/select-theme-default.css"/>
<link rel="stylesheet" href="css/owl.carousel.css"/>
<link rel="stylesheet" href="css/owl.theme.css"/>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
     <style>
        .label{
            display:inline-block;
            margin-bottom:5px;
            font-weight:300;
            font-size:13px;
            padding:15px;
            color:goldenrod;
        }
        .form-section{
    width:100%; background:#fff; border:1px solid #ddd; border-radius:3px; font-size:13px; color:#333; padding:5px; font-family: 'Open Sans', sans-serif;
}
        .fileUpload{
             width:100%; background:#fff; border:1px solid #ddd; border-radius:3px; font-size:13px; color:#333; padding:7px 9px; font-family: 'Open Sans', sans-serif;
        }
       .personal-details{
            width:100%; 
            float:left; 
            font-size:16px; 
            color:goldenrod; 
            font-weight:400; 
            text-align:left; 
            padding:9px 0 9px 15px; 
            margin:20px 0 23px 0; 
            background:#eee; 
            border:1px solid #e9e9e9; 
            font-family: 'Montserrat', sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
         <header>
<div id="top-strip">
	<div class="container">
		<ul class="pull-left social-icons">
			<li><a href="#" class="fa fa-google-plus"></a></li>
			<li><a href="#" class="fa fa-twitter"></a></li>
			<li><a href="#" class="fa fa-pinterest"></a></li>
			<li><a href="#" class="fa fa-dribbble"></a></li>
			<li><a href="#" class="fa fa-linkedin"></a></li>
			<li><a href="#" class="fa fa-facebook"></a></li>
		</ul>
        <div id="login-box" class='pull-right'>
            <a class='fa fa-phone'><span>+91-9818060989 | +91-9999654898</span></a>

        </div>
	</div>
</div>
</header>
<!-- /Header -->
       <div class="slider-section">
	<div id="premium-bar">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
                    <a class="navbar-brand" href="Default.aspx"><img src="img/logo.jpg" alt="logo"/></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="Default.aspx">Home</a></li>
                        <li><a href="About.aspx">About Us</a></li>
                        <li><a href="listing.aspx">Properties</a></li>
                        <li><a href="interiors.aspx">Interiors</a></li>
                        <li class="active"><a href="PostRequirement.aspx">Post Requirement</a></li>
                        <li><a href="Contactus.aspx">Contact Us</a></li>

                    </ul>
                </div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
			</nav>
		</div>
	</div>
	<!-- head-Section -->
	<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>Post Requirement</h2>
				</a>
			</div>
			
		</div>
	</div>
</div>
    <!-- Search-Section -->
    <div class="search-section">
       <div class="container">
		
			<div class="select-wrapper select-big" id='select-rent'>
				<p>
					 Looking For
				</p>
                <asp:DropDownList ID="PropertyTypeDropdown" runat="server" class='elselect'>
                    <asp:ListItem>Rent</asp:ListItem>
                    <asp:ListItem>Sale</asp:ListItem>
                   
                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big" id='select-property'>
				<p>
					 Property type
				</p>
                <asp:DropDownList ID="PurposeDropdown" runat="server" class='elselect'>
                    <asp:ListItem>Residential</asp:ListItem>
                    <asp:ListItem>Commercials</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big">
				<p>
					 Location
				</p>
                  <asp:DropDownList ID="LocationDropdown" runat="server" class='elselect'>
                    <asp:ListItem>All</asp:ListItem>
                       <asp:ListItem>Pitampura</asp:ListItem>
                                            <asp:ListItem>Rohini</asp:ListItem>
                                            <asp:ListItem>Netaji Subhas Place</asp:ListItem>
                                            <asp:ListItem>Paschim Vihar</asp:ListItem>
                                           
                                            <asp:ListItem>Yamuna Expressway</asp:ListItem>
                                            <asp:ListItem>Noida</asp:ListItem>
                                            <asp:ListItem>Gurgaon</asp:ListItem>
                    
                </asp:DropDownList>
				
			</div>
			
			
			
			<div class="select-wrapper select-big">
				<p>
					 Min Price
				</p>
                 <asp:DropDownList ID="MinPriceDropdown" runat="server" class='elselect'>
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="10000">₹ 10000</asp:ListItem>
                      <asp:ListItem Value="25000">₹ 25000</asp:ListItem>
                      <asp:ListItem Value="50000">₹ 50000</asp:ListItem>
                      <asp:ListItem Value="100000">₹ 100000</asp:ListItem>
                      <asp:ListItem Value="125000">₹ 125000</asp:ListItem>
                      <asp:ListItem Value="150000">₹ 150000</asp:ListItem>

                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big">
				<p>
					 Max Price
				</p>
				<asp:DropDownList ID="MaxPriceDropdown" runat="server" class='elselect'>
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="100000">₹ 100000</asp:ListItem>
                      <asp:ListItem Value="250000">₹ 250000</asp:ListItem>
                      <asp:ListItem Value="500000">₹ 500000</asp:ListItem>
                      <asp:ListItem Value="1000000">₹ 1000000</asp:ListItem>
                      <asp:ListItem Value="1250000">₹ 1250000</asp:ListItem>
                      <asp:ListItem Value="1500000">₹ 1500000</asp:ListItem>

                </asp:DropDownList>
			</div>
            <asp:Button ID="SearchButton" runat="server" Text="Search" class='yellow-btn' OnClick="SearchButton_Click" />
			
	
	</div>
    </div>

        <!-- content-Section -->
        <div class="content-section">
	<div class="container">
		
		<div class="col-lg-8 contact-info">
			<div class="inner-wrapper" style="margin-left:157px;">
				
				<p class='first-paragraph'>The following are the basic details we would require to make your property live.</p>
				<form class="form-contact" name="postrequirement" id="postrequirement" action="" method="post" target="_blank" onsubmit="return reqvalidation();" enctype="multipart/form-data">
                  <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label"> I want to <samp style="color:#F00">*</samp> : </label>
                </div>
                <div class="col-sm-8">
                  <select class="form-section">
                    <option value="">---- Select I want to ----</option>
                                        <option value="Pre Lease">Pre Lease</option>
                                        <option value="Rent">Rent</option>
                                        <option value="Sale">Sale</option>
                                        <option value="Purchase">Purchase</option>
                  </select>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Preferred City <samp style="color:#F00">*</samp> : </label>
                </div>
                <div class="col-sm-8">
                  <input id="city_id" name="city_id" class="form-section" type="text" placeholder=" City"/>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label"> Area <samp style="color:#F00">*</samp> : </label>
                </div>
                <div class="col-sm-8">
                  <input id="location_id" name="location_id" class="form-section" type="text" placeholder=" Area"/>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Looking for <samp style="color:#F00">*</samp> : </label>
                </div>
                <div class="col-sm-8">
                  <select id="category_id" name="category_id" class="form-section" onchange="selectcategory_id(this.value);">
                    <option value="">------ Select Looking for ------</option>
                                        <option value="Residential">Residential</option>
                                        <option value="Commercial">Commercial</option>
                                      </select>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Property Type <samp style="color:#F00">*</samp> : </label>
                </div>
                  
                <div class="col-sm-8">
                  <select id="pro_type_id" name="pro_type_id" class="form-section">
                    <option value="">------ Select Property Type ------</option>
                                        <option value="Apartments">Apartments</option>
                                        <option value="Independent House ">Independent House </option>
                                        <option value="Office Space ">Office Space </option>
                                        <option value="Office Space in IT Park ">Office Space in IT Park </option>
                                        <option value="Shop/Showroom ">Shop/Showroom </option>
                                        <option value="Space in Retail Mall ">Space in Retail Mall </option>
                                        <option value="Retail Shop ">Retail Shop </option>
                                      </select>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Sub Category : </label>
                </div>
                <div class="col-sm-8">
                  <select class="form-section" id="furnishing_id" name="furnishing_id">
                    <option value="">----Select Sub Category----</option>
                                        <option value="Furnished">Furnished</option>
                                        <option value="Semi Furnished">Semi Furnished</option>
                                        <option value="Unfurnished">Unfurnished</option>
                                        <option value="Fully Furnished">Fully Furnished</option>
                                      </select>
                </div>
              </div>
                     <div class="row form-listing" id="bhksss">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Bedrooms : </label>
                </div>
                <div class="col-sm-8">
                  <select class="form-section" name="bedrooms" id="bedrooms">
                    <option value="">------ Select Bedrooms ------</option>
                    <option value="1 BHK">1 BHK</option>
                    <option value="2 BHK">2 BHK</option>
                    <option value="3 BHK">3 BHK</option>
                    <option value="4 BHK">4 BHK</option>
                    <option value="5 BHK">5 BHK</option>
                    <option value="6 BHK">6 BHK</option>
                    <option value="7 BHK">7 BHK</option>
                    <option value="8 BHK">8 BHK</option>
                    <option value="9 BHK">9 BHK</option>
                    <option value="10 BHK">10 BHK</option>
                  </select>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Covered/Built-up area : </label>
                </div>
                <div class="col-sm-8">
                  <div class="row">
                    <div class="col-md-4 row-no-paddingRight">
                      <input type="text" id="minimumarea" name="minimumarea" placeholder="Minimum Area :" class="form-section">
                    </div>
                    <div class="col-md-4 row-no-paddingRight">
                      <input type="text" id="maximnum" name="maximnum" placeholder="Maximnum Area :" class="form-section">
                    </div>
                    <div class="col-md-4">
                      <select id="area_type" name="area_type" class="form-section">
                        <option value="">Select</option>
                        <option value="Sq.Ft.">Sq.Ft.</option>
                        <option value="Sq.Mtr.">Sq.Mtr.</option>
                        <option value="Sq.Yds.">Sq.Yds.</option>
                        <option value="Acres">Acres</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Budget : </label>
                </div>
                <div class="col-sm-8">
                  <select id="buy_budget" name="buy_budget" class="form-section">
                    <option value="">------ Select Budget ------</option>
                    <option value="50 Lacs">less than 50 Lacs </option>
                    <option value="50 - 99 Lacs">50 - 99 Lacs</option>
                    <option value="1 cr - 1.5 cr">1 cr - 1.5 cr</option>
                    <option value="1.5 - 2 cr">1.5 - 2 cr</option>
                    <option value="2 - 3 cr">2 - 3 cr</option>
                    <option value="3 - 10 cr">3 - 10 cr</option>
                    <option value="10 - 20 cr">10 - 20 cr</option>
                    <option value="20 - 50 cr">20 - 50 cr</option>
                    <option value="50+ cr">50+ cr</option>
                  </select>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Upload Photo: </label>
                </div>
                <div class="col-sm-8">
                  <input type="file" name="image" id="image" class="fileUpload"/>
                </div>
              </div>
              <div class="form-listing">
                <label for="form_name" class="label">Comments/ Overview :</label>
                <textarea id="description" name="description" class="form-section required" rows="3" placeholder="Comments/ Overview"></textarea>
              </div>
              <div class="row form-listing">
                <div class="personal-details">Your Personal Details</div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Full Name <samp style="color:#F00">*</samp> : </label>
                </div>
                <div class="col-sm-8">
                  <input id="full_name" name="full_name" class="form-section" type="text" placeholder=" Full Name">
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label">Email-ID <samp style="color:#F00">*</samp> : </label>
                </div>
                <div class="col-sm-8">
                  <input id="emailid" name="emailid" class="form-section" type="text" placeholder="Email-ID"/>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label"> Mob/Pho. <samp style="color:#F00">*</samp> : </label>
                </div>
                <div class="col-sm-8">
                  <input id="moblieno" name="moblieno" class="form-section" type="text" placeholder="Mob/Pho"/>
                </div>
              </div>
             <div class="row form-listing">
               <div class="col-sm-4">
                  <label for="form_name" class="label"> </label>
                </div>
                 <div class="col-sm-8">
                 <div class="radio-inline">
                      <input type="checkbox" name="agree" value="check" style="font-size:25px;"/>
                      I agree to Mohan Estates <a class="defaultLinks font-familyMontserrat" href="#" style="color:goldenrod">Terms of Use</a> and <a class="defaultLinks font-familyMontserrat" href="#" style="color:goldenrod;">Privacy Policy</a> and would like to receive communication through Email, Call or SMS. 
                    </div>
                </div>
              </div>
              <div class="row form-listing">
                <div class="col-sm-4">
                  <label for="form_name" class="label"> </label>
                </div>
                 <div class="col-sm-8">
                <input id="form_botcheck" name="form_botcheck" class="form-section" type="hidden" value="" />
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-dark btn-flat" onclick="return reqvalidation();" />
                <button type="reset" class="btn btn-default btn-flat btn-theme-colored">Reset</button>
                </div>
              </div>
              
              </div>
              
                
                </form>
				
				<div class="clearfix">
				</div>
			</div>
		</div>
        
		
	</div>
</div>











         <!-- footer-section -->
        <footer>
        <div class="container">
            <div class="col-md-3 footer-about">
                <a class="logo" href="#"><img src="img/footer-logo.jpg" alt="logo"/></a>
                <p>
                     MOHANS ESTATES DEVELOPERS has been a pioneer in the fields of Real Estate Development. In real estate our expertise lies
in the development and promotion of Farmhouses, Residential Plots /Buildings, Shopping Malls and CommercialBuildings.
                </p>
            </div>
            <div class="col-md-3 footer-recent-posts">
                <h3 class='footer-title'>ADDRESS</h3>
                <ul>
                    <li>
                        <a href="#">
                            <i class="fa fa-map-marker"></i>  Ground Floor-6, Dmall,
                            Netaji subhash palace, New Delhi
                        </a>
                    </li>
                    <li><a href="#"><i class="fa fa-phone"></i>+91-98 1806 0989</a></li>
                    <li><a href="#"><i class="fa fa-globe"></i>mohansestates.com</a></li>
                    <li><a href="#"><i class="fa fa-globe"></i>dmall.in.net</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>contact@mohansestates.com</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-contact-info">
                <h3 class='footer-title'>WE ARE SOCIAL</h3>
                <p class="website-number">
                    <i class="fa fa-facebook"></i> Facebook
                </p>
                <p class="website-email">
                    <i class="fa fa-twitter"></i> Twitter
                </p>
                <p class="website-fax">
                    <i class="fa fa-google-plus"></i>Google Plus
                </p>
            </div>
            <div class="col-md-3 footer-recent-posts">
                <h3 class='footer-title'>Other Links</h3>
                <ul>
                    <li>
                        <a>About Us</a>
                    </li>
                    <li>
                        <a>Contact Us</a>
                    </li>

                    <li>
                        <a>Career</a>
                    </li>
                    <li>
                        <a>Terms and Conditions</a>
                    </li>
                    +<li>
                        <a>Privacy Policies</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
        <div class="bottom-strip">
    <div class="container">
        <div class="col-md-4">
            <p class='pull-left'>
                © 2017 Mohan Estates Developers, All Rights Reserved
            </p>
        </div>
        <div class="col-md-4" class='bottom-strip-middle'>
            <a href="#top" class='fa fa-arrow-circle-up' id='backtop-btn'></a>
        </div>
        <div class="col-md-4">
            <ul class="social-icons">
                <li><a href="#" class="fa fa-google-plus"></a></li>
                <li><a href="#" class="fa fa-twitter"></a></li>
                <li><a href="#" class="fa fa-pinterest"></a></li>
                <li><a href="#" class="fa fa-dribbble"></a></li>
                <li><a href="#" class="fa fa-linkedin"></a></li>
                <li><a href="#" class="fa fa-facebook"></a></li>
            </ul>
        </div>
    </div>
</div>
        <!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/select.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAR1B1lSLpfOjpUsnDMS9c6FTo3eQJSTUs&sensor=false"></script>
<script type="text/javascript" src="js/script.js"></script>
       <%-- <script type="text/javascript" src="js/Js/app.js"></script>
        <script type="text/javascript" src="js/Js/chosen.jquery.js"></script>
        <script type="text/javascript" src="js/Js/jquery.parallax.js"></script>
        <script type="text/javascript" src="js/Js/jquery.scrolly.min.js"></script>
        <script type="text/javascript" src="js/Js/skel.min.js"></script>
        <script type="text/javascript" src="js/Js/util.js"></script>
        <script type="text/javascript" src="js/Js/jquery.min.js"></script>
        <script type="text/javascript" src="js/Js/scrollex.js"></script>
        <script type="text/javascript" src="js/Js/bootstrap.min.js"></script>--%>


<script>
    $(document).ready(function () {
        //Google Map
        var map;
        var marker;
        var MyMarker;
        function initialize() {
            var map_canvas = document.getElementById('map');
            var myLatlng = new google.maps.LatLng(28.6928764, 77.1526366);
            var mapOptions = {
                center: myLatlng,
                zoom: 17,
                scrollwheel: false,
                styles: [{ "featureType": "landscape", "stylers": [{ "saturation": -100 }, { "lightness": 65 }, { "visibility": "on" }] }, { "featureType": "poi", "stylers": [{ "saturation": -100 }, { "lightness": 51 }, { "visibility": "simplified" }] }, { "featureType": "road.highway", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "road.arterial", "stylers": [{ "saturation": -100 }, { "lightness": 30 }, { "visibility": "on" }] }, { "featureType": "road.local", "stylers": [{ "saturation": -100 }, { "lightness": 40 }, { "visibility": "on" }] }, { "featureType": "transit", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "administrative.province", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "labels", "stylers": [{ "visibility": "on" }, { "lightness": -25 }, { "saturation": -100 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "hue": "#ffff00" }, { "lightness": -25 }, { "saturation": -97 }] }],
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(map_canvas, mapOptions);
            TestMarker();
        }
        // Function for adding a marker to the page.
        function addMarker(location) {
            marker = new google.maps.Marker({
                position: location,
                icon: 'img/28_marker.png',
                map: map
            });
        }
        // Testing the addMarker function
        function TestMarker() {
            MyMarker = new google.maps.LatLng(28.6928764, 77.1526366);
            addMarker(MyMarker);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    });
	</script>




    </form>
</body>
</html>
