﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="listing.aspx.cs" Inherits="listing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <!--Page Title-->
<title>Mohan Estates Developers</title>
<!--Meta Tags-->
<meta charset="UTF-8">
<meta name="author" content="">
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<!-- Set Viewport-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css"/>
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/select-theme-default.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <header>
<div id="top-strip">
	<div class="container">
		<ul class="pull-left social-icons">
			<li><a href="#" class="fa fa-google-plus"></a></li>
			<li><a href="#" class="fa fa-twitter"></a></li>
			<li><a href="#" class="fa fa-pinterest"></a></li>
			<li><a href="#" class="fa fa-dribbble"></a></li>
			<li><a href="#" class="fa fa-linkedin"></a></li>
			<li><a href="#" class="fa fa-facebook"></a></li>
		</ul>
        <div id="login-box" class='pull-right'>
            <a class='fa fa-phone'><span>+91-9818060989 | +91-9999654898</span></a>

        </div>
	</div>
</div>
</header>
<!-- /Header -->
<div class="slider-section">
    <div id="premium-bar">
        <div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><img src="img/logo.jpg" alt="logo" /></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Home</a></li>
                            <li><a href="About.aspx">About Us</a></li>
                            <li class="active"><a href="listing.aspx">Properties</a></li>
                            <li><a href="interiors.aspx">Interiors</a></li>
                            <li><a href="PostRequirement.aspx">Post Requirement</a></li>
                            <li><a href="Contactus.aspx">Contact Us</a></li>

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>
	<!-- head-Section -->
	<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>property listing</h2>
				</a>
			</div>
			
		</div>
	</div>
</div>
    <!-- Search-Section -->
    <div class="search-section">
       <div class="container">
		
			<div class="select-wrapper select-big" id='select-rent'>
				<p>
					 Looking For
				</p>
                <asp:DropDownList ID="PropertyTypeDropdown" runat="server" class='elselect'>
                    <asp:ListItem>Rent</asp:ListItem>
                    <asp:ListItem>Sale</asp:ListItem>
                   
                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big" id='select-property'>
				<p>
					 Property type
				</p>
                <asp:DropDownList ID="PurposeDropdown" runat="server" class='elselect'>
                    <asp:ListItem>Residential</asp:ListItem>
                    <asp:ListItem>Commercials</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big">
				<p>
					 Location
				</p>
                  <asp:DropDownList ID="LocationDropdown" runat="server" class='elselect'>
                    <asp:ListItem>All</asp:ListItem>
                       <asp:ListItem>Pitampura</asp:ListItem>
                                            <asp:ListItem>Rohini</asp:ListItem>
                                            <asp:ListItem>Netaji Subhas Place</asp:ListItem>
                                            <asp:ListItem>Paschim Vihar</asp:ListItem>
                                           
                                            <asp:ListItem>Yamuna Expressway</asp:ListItem>
                                            <asp:ListItem>Noida</asp:ListItem>
                                            <asp:ListItem>Gurgaon</asp:ListItem>
                    
                </asp:DropDownList>
				
			</div>
			
			
			
			<div class="select-wrapper select-big">
				<p>
					 Min Price
				</p>
                 <asp:DropDownList ID="MinPriceDropdown" runat="server" class='elselect' >
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="10000">₹ 10000</asp:ListItem>
                      <asp:ListItem Value="25000">₹ 25000</asp:ListItem>
                      <asp:ListItem Value="50000">₹ 50000</asp:ListItem>
                      <asp:ListItem Value="100000">₹ 100000</asp:ListItem>
                      <asp:ListItem Value="125000">₹ 125000</asp:ListItem>
                      <asp:ListItem Value="150000">₹ 150000</asp:ListItem>

                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big">
				<p>
					 Max Price
				</p>
				<asp:DropDownList ID="MaxPriceDropdown" runat="server" class='elselect'>
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="100000">₹ 100000</asp:ListItem>
                      <asp:ListItem Value="250000">₹ 250000</asp:ListItem>
                      <asp:ListItem Value="500000">₹ 500000</asp:ListItem>
                      <asp:ListItem Value="1000000">₹ 1000000</asp:ListItem>
                      <asp:ListItem Value="1250000">₹ 1250000</asp:ListItem>
                      <asp:ListItem Value="1500000">₹ 1500000</asp:ListItem>

                </asp:DropDownList>
			</div>
            <asp:Button ID="SearchButton" runat="server" Text="Search" class='yellow-btn' OnClick="SearchButton_Click" />
			
	
	</div>
    </div>
<!-- content-Section -->
<div class="content-section">
	<div class="container container1">
		<div class="row listings-items-wrapper" id="PropertyContainer" runat="server">
            
			
			
			
			
		
			<!-- /Single-item -->
		</div>
		<%--<div class="pagination-wrapper">
			<ul class="pagination">
				<li><a href="#">&laquo;</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">&raquo;</a></li>
			</ul>
		</div>--%>
	</div>
</div>
    <!-- footer-section -->
    <footer>
        <div class="container">
           <div class="col-md-3 footer-about">
                <a class="logo" href="#"><img src="img/footer-logo.jpg" alt="logo"/></a>
                <p>
                     MOHANS ESTATES DEVELOPERS has been a pioneer in the fields of Real Estate Development. In real estate our expertise lies
in the development and promotion of Farmhouses, Residential Plots /Buildings, Shopping Malls and CommercialBuildings.
                </p>
            </div>
            <div class="col-md-3 footer-recent-posts">
                <h3 class='footer-title'>ADDRESS</h3>
                <ul>
                    <li>
                        <a href="#">
                            <i class="fa fa-map-marker"></i>  Ground Floor-6, Dmall,
                            Netaji subhash palace, New Delhi
                        </a>
                    </li>
                    <li><a href="#"><i class="fa fa-phone"></i>+91-98 1806 0989</a></li>
                    <li><a href="#"><i class="fa fa-globe"></i>mohansestates.com</a></li>
                    <li><a href="#"><i class="fa fa-globe"></i>dmall.in.net</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>contact@mohansestates.com</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-contact-info">
                <h3 class='footer-title'>WE ARE SOCIAL</h3>
                <p class="website-number">
                    <i class="fa fa-facebook"></i> Facebook
                </p>
                <p class="website-email">
                    <i class="fa fa-twitter"></i> Twitter
                </p>
                <p class="website-fax">
                    <i class="fa fa-google-plus"></i>Google Plus
                </p>
            </div>
            <div class="col-md-3 footer-recent-posts">
                <h3 class='footer-title'>Other Links</h3>
                <ul>
                    <li>
                        <a>About Us</a>
                    </li>
                    <li>
                        <a>Contact Us</a>
                    </li>

                    <li>
                        <a>Career</a>
                    </li>
                    <li>
                        <a>Terms and Conditions</a>
                    </li>
                    <li>
                        <a>Privacy Policies</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
<div class="bottom-strip">
	<div class="container">
		<div class="col-md-4">
            <p class='pull-left'>
                © 2017 Mohan Estates Developers, All Rights Reserved
            </p>
		</div>
		<div class="col-md-4" class='bottom-strip-middle'>
			<a href="#top" class='fa fa-arrow-circle-up' id='backtop-btn'></a>
		</div>
		<div class="col-md-4">
			<ul class="social-icons">
				<li><a href="#" class="fa fa-google-plus"></a></li>
				<li><a href="#" class="fa fa-twitter"></a></li>
				<li><a href="#" class="fa fa-pinterest"></a></li>
				<li><a href="#" class="fa fa-dribbble"></a></li>
				<li><a href="#" class="fa fa-linkedin"></a></li>
				<li><a href="#" class="fa fa-facebook"></a></li>
			</ul>
		</div>
	</div>
</div>
<!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/select.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
    </div>
    </form>
</body>
</html>
