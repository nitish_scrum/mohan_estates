﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections;

/// <summary>
/// Summary description for AdminDao
/// </summary>
public class AdminDao
{
    public AdminDao()
    {
        //
        // TODO: Add constructor logic here
        //
    }
     
    public int AdminLogin(string username,string password)
    {
        int flag = 0;

        string query = "select * from admin_login where username = '"+username+"' and password = '"+password+"'";
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        if (con.State != System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        MySqlCommand cmd = new MySqlCommand(query, con);
        MySqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            flag = 1;
        }
        con.Close();
        return flag;
    }

    public bool AddNewProperty(Property property)
    {
        bool flag = false;
        string query = "insert into property (title,purpose,description,price,covered_area,super_area,bedrooms,bathrooms,reception,garage,features,image1,image2," +
            "image3,image4,image5,address,location,preview_video,property_type,workstation,meeting_room,confrence_room,pantry_kitchen,status,category,furnishing) values (@title,@purpose,@description,@price,@covered_area,@super_area,@bedrooms,@bathrooms,@reception," +
            "@garage,@features,@image1,@image2,@image3,@image4,@image5,@address,@location,@preview_video,@property_type,@workstation,@confrence_room,@meeting_room,@pantry_kitchen,@status,@category,@furnishing)";
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        MySqlCommand cmd = new MySqlCommand(query, con);



        cmd.Parameters.Add("@title", MySqlDbType.VarChar);
        cmd.Parameters.Add("@purpose", MySqlDbType.VarChar);
        cmd.Parameters.Add("@description", MySqlDbType.VarChar);
        cmd.Parameters.Add("@price", MySqlDbType.Decimal);
        cmd.Parameters.Add("@covered_area", MySqlDbType.Decimal);
        cmd.Parameters.Add("@super_area", MySqlDbType.Decimal);
        cmd.Parameters.Add("@status", MySqlDbType.VarChar);
        cmd.Parameters.Add("@category", MySqlDbType.VarChar);
        cmd.Parameters.Add("@bedrooms", MySqlDbType.Int32);
        cmd.Parameters.Add("@bathrooms", MySqlDbType.Int32);
        cmd.Parameters.Add("@garage", MySqlDbType.Int32);
        cmd.Parameters.Add("@workstation", MySqlDbType.Int32);
        cmd.Parameters.Add("@meeting_room", MySqlDbType.Int32);
        cmd.Parameters.Add("@pantry_kitchen", MySqlDbType.Int32);
        cmd.Parameters.Add("@confrence_room", MySqlDbType.Int32);
        cmd.Parameters.Add("@features", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image1", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image2", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image3", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image4", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image5", MySqlDbType.VarChar);
        cmd.Parameters.Add("@address", MySqlDbType.VarChar);
        cmd.Parameters.Add("@location", MySqlDbType.VarChar);
        cmd.Parameters.Add("@preview_video", MySqlDbType.VarChar);
        cmd.Parameters.Add("@reception", MySqlDbType.VarChar);
        cmd.Parameters.Add("@property_type", MySqlDbType.VarChar);
        cmd.Parameters.Add("@furnishing", MySqlDbType.VarChar);

        cmd.Parameters["@title"].Value = property.Title;
        cmd.Parameters["@purpose"].Value = property.Purpose;
        cmd.Parameters["@description"].Value = property.Description;
        cmd.Parameters["@price"].Value = property.Price;
        cmd.Parameters["@covered_area"].Value = property.CoveredArea;
        cmd.Parameters["@superarea"].Value = property.SuperArea;
        cmd.Parameters["@bedrooms"].Value = property.Bedrooms;
        cmd.Parameters["@bathrooms"].Value = property.Bathrooms;
        cmd.Parameters["@garage"].Value = property.Garage;
        cmd.Parameters["@features"].Value = property.Features;
        cmd.Parameters["@image1"].Value = property.Image1;
        cmd.Parameters["@image2"].Value = property.Image2;
        cmd.Parameters["@image3"].Value = property.Image3;
        cmd.Parameters["@image4"].Value = property.Image4;
        cmd.Parameters["@image5"].Value = property.Image5;
        cmd.Parameters["@address"].Value = property.Address;
        cmd.Parameters["@location"].Value = property.Location;
        cmd.Parameters["@preview_video"].Value = property.PreviewVideo;
        cmd.Parameters["@reception"].Value = property.Reception;
        cmd.Parameters["@property_type"].Value = property.PropertyType;
        cmd.Parameters["@workstation"].Value = property.Workstation;
        cmd.Parameters["@confrence_room"].Value = property.ConfrenceRoom;
        cmd.Parameters["@meeting_room"].Value = property.MeetingRoom;
        cmd.Parameters["@pantry_kitchen"].Value = property.PantryKitchen;
        cmd.Parameters["@status"].Value = property.Status;
        cmd.Parameters["@category"].Value = property.Category;
        cmd.Parameters["@furnishing"].Value = property.Furnishing;

        if(con.State!=System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        int z = cmd.ExecuteNonQuery();
        if (z > 0)
            flag = true;
        con.Close();
        return flag;
    }



    public bool UpdateProperty(Property property)
    {
        bool flag = false;
        string query = "update property set title = @title, purpose = @purpose , description = @description,price=@price," +
            "covered_area=@covered_area,super_area=@super_area,bedrooms=@bedrooms,bathrooms=@bathrooms,reception=@reception,garage=@garage,features=@features," +
            "image1=@image1,image2=@image2,image3=@image3,image4=@image4,image5=@image5,address=@address,location=@location,preview_video=@preview_video,property_type=@property_type,workstation=@workstation,meeting_room=@meeting_room,confrence_room=@confrence_room,pantry_kitchen=@pantry_kitchen,status=@status,category=@category,furnishing=@furnishing" +
            " where property_id=@property_id";
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        MySqlCommand cmd = new MySqlCommand(query, con);


        cmd.Parameters.Add("@property_id", MySqlDbType.Int32);
        cmd.Parameters.Add("@title", MySqlDbType.VarChar);
        cmd.Parameters.Add("@purpose", MySqlDbType.VarChar);
        cmd.Parameters.Add("@description", MySqlDbType.VarChar);
        cmd.Parameters.Add("@price", MySqlDbType.Decimal);
        cmd.Parameters.Add("@covered_area", MySqlDbType.Decimal);
        cmd.Parameters.Add("@super_area", MySqlDbType.Decimal);
        cmd.Parameters.Add("@bedrooms", MySqlDbType.Int32);
        cmd.Parameters.Add("@bathrooms", MySqlDbType.Int32);
        cmd.Parameters.Add("@garage", MySqlDbType.Int32);
        cmd.Parameters.Add("@features", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image1", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image2", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image3", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image4", MySqlDbType.VarChar);
        cmd.Parameters.Add("@image5", MySqlDbType.VarChar);
        cmd.Parameters.Add("@address", MySqlDbType.VarChar);
        cmd.Parameters.Add("@location", MySqlDbType.VarChar);
        cmd.Parameters.Add("@preview_video", MySqlDbType.VarChar);
        cmd.Parameters.Add("@reception", MySqlDbType.VarChar);
        cmd.Parameters.Add("@property_type", MySqlDbType.VarChar);
        cmd.Parameters.Add("@workstation", MySqlDbType.Int32);
        cmd.Parameters.Add("@meeting_room", MySqlDbType.Int32);
        cmd.Parameters.Add("@pantry_kitchen", MySqlDbType.Int32);
        cmd.Parameters.Add("@confrence_room", MySqlDbType.Int32);
        cmd.Parameters.Add("@status", MySqlDbType.VarChar);
        cmd.Parameters.Add("@category", MySqlDbType.VarChar);
        cmd.Parameters.Add("@furnishing", MySqlDbType.VarChar);

        cmd.Parameters["@property_id"].Value = property.PropertyId;
        cmd.Parameters["@title"].Value = property.Title;
        cmd.Parameters["@purpose"].Value = property.Purpose;
        cmd.Parameters["@description"].Value = property.Description;
        cmd.Parameters["@price"].Value = property.Price;
        cmd.Parameters["@covered_area"].Value = property.CoveredArea;
        cmd.Parameters["@super_area"].Value = property.SuperArea;
        cmd.Parameters["@bedrooms"].Value = property.Bedrooms;
        cmd.Parameters["@bathrooms"].Value = property.Bathrooms;
        cmd.Parameters["@garage"].Value = property.Garage;
        cmd.Parameters["@features"].Value = property.Features;
        cmd.Parameters["@image1"].Value = property.Image1;
        cmd.Parameters["@image2"].Value = property.Image2;
        cmd.Parameters["@image3"].Value = property.Image3;
        cmd.Parameters["@image4"].Value = property.Image4;
        cmd.Parameters["@image5"].Value = property.Image5;
        cmd.Parameters["@address"].Value = property.Address;
        cmd.Parameters["@location"].Value = property.Location;
        cmd.Parameters["@preview_video"].Value = property.PreviewVideo;
        cmd.Parameters["@reception"].Value = property.Reception;
        cmd.Parameters["@property_type"].Value = property.PropertyType;
        cmd.Parameters["@workstation"].Value = property.Workstation;
        cmd.Parameters["@confrence_room"].Value = property.ConfrenceRoom;
        cmd.Parameters["@meeting_room"].Value = property.MeetingRoom;
        cmd.Parameters["@pantry_kitchen"].Value = property.PantryKitchen;
        cmd.Parameters["@status"].Value = property.Status;
        cmd.Parameters["@category"].Value = property.Category;
        cmd.Parameters["@furnishing"].Value = property.Furnishing;


        if (con.State != System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        int z = cmd.ExecuteNonQuery();
        if (z > 0)
            flag = true;
        con.Close();
        return flag;
    }

    public ArrayList GetAllPropertyByPurpose(String purpose)
    {
        ArrayList PropertyList = new ArrayList();
        string query = "select * from property where purpose = '" + purpose + "'";
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        if (con.State != System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        MySqlCommand cmd = new MySqlCommand(query, con);
        MySqlDataReader reader = cmd.ExecuteReader();
        while(reader.Read())
        {
            int propertyId = reader.GetInt32(0);
            string title = reader.GetString(1);
          
            string description = reader.GetString(3);
            double price = reader.GetDouble(4);
            double coveredArea = reader.GetDouble(5);

            int bedrooms = 0;
            if (!reader.IsDBNull(6))
            {
                bedrooms = reader.GetInt32(6);
            }


            int reception = 0;
            if (!reader.IsDBNull(7))
            {
                reception = reader.GetInt32(7);
            }

            int bathrooms = 0;
            if (!reader.IsDBNull(8))
            {
                bathrooms = reader.GetInt32(8);
            }

            int garage = 0;
            if (!reader.IsDBNull(9))
            {
                garage = reader.GetInt32(9);
            }

            string features = "";
            if (!reader.IsDBNull(10))
            {
                features = reader.GetString(10);
            }


            string image1 = "";
            if (!reader.IsDBNull(11))
            {
                image1 = reader.GetString(11);
            }

            string image2 = "";
            if (!reader.IsDBNull(12))
            {
                image2 = reader.GetString(12);
            }

            string image3 = "";
            if (!reader.IsDBNull(13))
            {
                image3 = reader.GetString(13);
            }

            string image4 = "";
            if (!reader.IsDBNull(14))
            {
                image4 = reader.GetString(14);
            }

            string image5 = "";
            if (!reader.IsDBNull(15))
            {
                image5 = reader.GetString(15);
            }

            string address = "";
            if (!reader.IsDBNull(16))
            {
                address = reader.GetString(16);
            }

            string location = "";
            if (!reader.IsDBNull(17))
            {
                location = reader.GetString(17);
            }

            string previewVideo = "";
            if (!reader.IsDBNull(18))
            {
                previewVideo = reader.GetString(18);
            }

            string propertyType = "";
            if (!reader.IsDBNull(19))
            {
                propertyType = reader.GetString(19);
            }

            int workstation = 0;
            if (!reader.IsDBNull(20))
            {
                workstation = reader.GetInt32(20);
            }
            int pantryKitchen = 0;
            if (!reader.IsDBNull(21))
            {
                pantryKitchen = reader.GetInt32(21);
            }

            int confrenceRoom = 0;
            if (!reader.IsDBNull(22))
            {
                confrenceRoom = reader.GetInt32(22);
            }

            int meetingRoom = 0;
            if (!reader.IsDBNull(23))
            {
                meetingRoom = reader.GetInt32(23);
            }

            string status = "";
            if (!reader.IsDBNull(24))
            {
                status = reader.GetString(24);
            }

            string category = "";
            if (!reader.IsDBNull(26))
            {
                category = reader.GetString(26);
            }

            string furnishing = "";
            if (!reader.IsDBNull(25))
            {
                furnishing = reader.GetString(25);
            }

            double superArea = 0;
            if (!reader.IsDBNull(27))
            {
                superArea = reader.GetDouble(27);
            }
            Property property = new Property(propertyId, title, purpose, description, price, coveredArea, superArea, bedrooms, bathrooms,
                garage, reception, workstation, confrenceRoom, meetingRoom, pantryKitchen, features, image1, image2, image3, image4, image5, address, location, previewVideo, propertyType, status, category, furnishing);
            PropertyList.Add(property);
        }
        con.Close();
        return PropertyList;
    }
    public System.Data.DataTable GetAllPropertyDataTableByPurpose(String purpose)
    {
        System.Data.DataTable PropertyList = new System.Data.DataTable();
        string query = "select * from property where purpose = '" + purpose + "'";
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        if (con.State != System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        MySqlCommand cmd = new MySqlCommand(query, con);
        MySqlDataReader reader = cmd.ExecuteReader();
        PropertyList.Load(reader);
        con.Close();
        return PropertyList;
    }
    public ArrayList GetAllProperty()
    {
        ArrayList PropertyList = new ArrayList();
        string query = "select * from property";
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        if (con.State != System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        MySqlCommand cmd = new MySqlCommand(query, con);
        MySqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {

            int propertyId = reader.GetInt32(0);
            string title = reader.GetString(1);
            string purpose = reader.GetString(2);
            string description = reader.GetString(3);
            double price = reader.GetDouble(4);
            double coveredArea = reader.GetDouble(5);

            int bedrooms = 0;
            if (!reader.IsDBNull(6))
            {
                bedrooms = reader.GetInt32(6);
            }


            int reception = 0;
            if (!reader.IsDBNull(7))
            {
                reception = reader.GetInt32(7);
            }

            int bathrooms = 0;
            if (!reader.IsDBNull(8))
            {
                bathrooms = reader.GetInt32(8);
            }

            int garage = 0;
            if (!reader.IsDBNull(9))
            {
                garage = reader.GetInt32(9);
            }

            string features = "";
            if (!reader.IsDBNull(10))
            {
                features = reader.GetString(10);
            }


            string image1 = "";
            if (!reader.IsDBNull(11))
            {
                image1 = reader.GetString(11);
            }

            string image2 = "";
            if (!reader.IsDBNull(12))
            {
                image2 = reader.GetString(12);
            }

            string image3 = "";
            if (!reader.IsDBNull(13))
            {
                image3 = reader.GetString(13);
            }

            string image4 = "";
            if (!reader.IsDBNull(14))
            {
                image4 = reader.GetString(14);
            }

            string image5 = "";
            if (!reader.IsDBNull(15))
            {
                image5 = reader.GetString(15);
            }

            string address = "";
            if (!reader.IsDBNull(16))
            {
                address = reader.GetString(16);
            }

            string location = "";
            if (!reader.IsDBNull(17))
            {
                location = reader.GetString(17);
            }

            string previewVideo = "";
            if (!reader.IsDBNull(18))
            {
                previewVideo = reader.GetString(18);
            }

            string propertyType = "";
            if (!reader.IsDBNull(19))
            {
                propertyType = reader.GetString(19);
            }

            int workstation = 0;
            if (!reader.IsDBNull(20))
            {
                workstation = reader.GetInt32(20);
            }
            int pantryKitchen = 0;
            if (!reader.IsDBNull(21))
            {
                pantryKitchen = reader.GetInt32(21);
            }

            int confrenceRoom = 0;
            if (!reader.IsDBNull(22))
            {
                confrenceRoom = reader.GetInt32(22);
            }

            int meetingRoom = 0;
            if (!reader.IsDBNull(23))
            {
                meetingRoom = reader.GetInt32(23);
            }

            string status = "";
            if (!reader.IsDBNull(24))
            {
                status = reader.GetString(24);
            }

            string category = "";
            if (!reader.IsDBNull(26))
            {
                category = reader.GetString(26);
            }

            string furnishing = "";
            if (!reader.IsDBNull(25))
            {
                furnishing = reader.GetString(25);
            }

            double superArea = 0;
            if (!reader.IsDBNull(27))
            {
                superArea = reader.GetDouble(27);
            }
            Property property = new Property(propertyId, title, purpose, description, price, coveredArea, superArea, bedrooms, bathrooms,
                garage, reception, workstation, confrenceRoom, meetingRoom, pantryKitchen, features, image1, image2, image3, image4, image5, address, location, previewVideo, propertyType, status, category, furnishing);
            PropertyList.Add(property);
        }
        con.Close();
        return PropertyList;
    }

    public ArrayList SearchProperty(string query)
    {
        ArrayList PropertyList = new ArrayList();
        
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        if (con.State != System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        MySqlCommand cmd = new MySqlCommand(query, con);
        MySqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {

            int propertyId = reader.GetInt32(0);
            string title = reader.GetString(1);
            string purpose = reader.GetString(2);
            string description = reader.GetString(3);
            double price = reader.GetDouble(4);
            double coveredArea = reader.GetDouble(5);

            int bedrooms = 0;
            if (!reader.IsDBNull(6))
            {
                bedrooms = reader.GetInt32(6);
            }

            int reception = 0;
            if (!reader.IsDBNull(7))
            {
                reception = reader.GetInt32(7);
            }

            int bathrooms = 0;
            if (!reader.IsDBNull(8))
            {
                bathrooms = reader.GetInt32(8);
            }

            int garage = 0;
            if (!reader.IsDBNull(9))
            {
                garage = reader.GetInt32(9);
            }

            string features = "";
            if (!reader.IsDBNull(10))
            {
                features = reader.GetString(10);
            }


            string image1 = "";
            if (!reader.IsDBNull(11))
            {
                image1 = reader.GetString(11);
            }

            string image2 = "";
            if (!reader.IsDBNull(12))
            {
                image2 = reader.GetString(12);
            }

            string image3 = "";
            if (!reader.IsDBNull(13))
            {
                image3 = reader.GetString(13);
            }

            string image4 = "";
            if (!reader.IsDBNull(14))
            {
                image4 = reader.GetString(14);
            }

            string image5 = "";
            if (!reader.IsDBNull(15))
            {
                image5 = reader.GetString(15);
            }

            string address = "";
            if (!reader.IsDBNull(16))
            {
                address = reader.GetString(16);
            }

            string location = "";
            if (!reader.IsDBNull(17))
            {
                location = reader.GetString(17);
            }

            string previewVideo = "";
            if (!reader.IsDBNull(18))
            {
                previewVideo = reader.GetString(18);
            }


            string propertyType = "";
            if (!reader.IsDBNull(19))
            {
                propertyType = reader.GetString(19);
            }


            int workstation = 0;
            if (!reader.IsDBNull(20))
            {
                workstation = reader.GetInt32(20);
            }
            int pantryKitchen = 0;
            if (!reader.IsDBNull(21))
            {
                pantryKitchen = reader.GetInt32(21);
            }

            int confrenceRoom = 0;
            if (!reader.IsDBNull(22))
            {
                confrenceRoom = reader.GetInt32(22);
            }

            int meetingRoom = 0;
            if (!reader.IsDBNull(23))
            {
                meetingRoom = reader.GetInt32(23);
            }

            string status = "";
            if (!reader.IsDBNull(24))
            {
                status = reader.GetString(24);
            }

            string category = "";
            if (!reader.IsDBNull(26))
            {
                category = reader.GetString(26);
            }

            string furnishing = "";
            if (!reader.IsDBNull(25))
            {
                furnishing = reader.GetString(25);
            }

            double superArea = 0;
            if (!reader.IsDBNull(27))
            {
                superArea = reader.GetDouble(27);
            }
            Property property = new Property(propertyId, title, purpose, description, price, coveredArea, superArea, bedrooms, bathrooms,
                garage, reception, workstation, confrenceRoom, meetingRoom, pantryKitchen, features, image1, image2, image3, image4, image5, address, location, previewVideo, propertyType, status, category, furnishing);
            PropertyList.Add(property);
        }
        con.Close();
        return PropertyList;
    }

    public Property GetPropertyDetailsById(int propertyId)
    {
        Property property = null;
        string query = "select * from property where property_id = "+propertyId;
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        if (con.State != System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        MySqlCommand cmd = new MySqlCommand(query, con);
        MySqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            string title = reader.GetString(1);
            string purpose = reader.GetString(2);
            string description = reader.GetString(3);
            double price = reader.GetDouble(4);
            double coveredArea = reader.GetDouble(5);

            int bedrooms = 0;
            if (!reader.IsDBNull(6))
            {
                bedrooms = reader.GetInt32(6);
            }


            int reception = 0;
            if (!reader.IsDBNull(7))
            {
                reception = reader.GetInt32(7);
            }

            int bathrooms = 0;
            if (!reader.IsDBNull(8))
            {
                bathrooms = reader.GetInt32(8);
            }

            int garage = 0;
            if (!reader.IsDBNull(9))
            {
                garage = reader.GetInt32(9);
            }

            string features = "";
            if (!reader.IsDBNull(10))
            {
                features = reader.GetString(10);
            }


            string image1 = "";
            if (!reader.IsDBNull(11))
            {
                image1 = reader.GetString(11);
            }

            string image2 = "";
            if (!reader.IsDBNull(12))
            {
                image2 = reader.GetString(12);
            }

            string image3 = "";
            if (!reader.IsDBNull(13))
            {
                image3 = reader.GetString(13);
            }

            string image4 = "";
            if (!reader.IsDBNull(14))
            {
                image4 = reader.GetString(14);
            }

            string image5 = "";
            if (!reader.IsDBNull(15))
            {
                image5 = reader.GetString(15);
            }

            string address = "";
            if (!reader.IsDBNull(16))
            {
                address = reader.GetString(16);
            }

            string location = "";
            if (!reader.IsDBNull(17))
            {
                location = reader.GetString(17);
            }

            string previewVideo = "";
            if (!reader.IsDBNull(18))
            {
                previewVideo = reader.GetString(18);
            }


            string propertyType = "";
            if (!reader.IsDBNull(19))
            {
                propertyType = reader.GetString(19);
            }


            int workstation = 0;
            if (!reader.IsDBNull(20))
            {
                workstation = reader.GetInt32(20);
            }
            int pantryKitchen = 0;
            if (!reader.IsDBNull(21))
            {
                pantryKitchen = reader.GetInt32(21);
            }

            int confrenceRoom = 0;
            if (!reader.IsDBNull(22))
            {
                confrenceRoom = reader.GetInt32(22);
            }

            int meetingRoom = 0;
            if (!reader.IsDBNull(23))
            {
                meetingRoom = reader.GetInt32(23);
            }

            string status = "";
            if (!reader.IsDBNull(24))
            {
                status = reader.GetString(24);
            }

            string category = "";
            if (!reader.IsDBNull(26))
            {
                category = reader.GetString(26);
            }

            string furnishing = "";
            if (!reader.IsDBNull(25))
            {
                furnishing = reader.GetString(25);
            }



            double superArea = 0;
            if (!reader.IsDBNull(27))
            {
                superArea = reader.GetDouble(27);
            }
             property = new Property(propertyId, title, purpose, description, price, coveredArea, superArea, bedrooms, bathrooms,
                garage, reception, workstation, confrenceRoom, meetingRoom, pantryKitchen, features, image1, image2, image3, image4, image5, address, location, previewVideo, propertyType, status, category, furnishing);


        }
        con.Close();
        return property;
    }

    public ArrayList GetRecentProperty()
    {
        ArrayList PropertyList = new ArrayList();
        string query = "select * from property property_id order by property_id desc limit 6;";
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        if (con.State != System.Data.ConnectionState.Open)
        {
            con.Open();
        }
        MySqlCommand cmd = new MySqlCommand(query, con);
        MySqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            int propertyId = reader.GetInt32(0);
            string title = reader.GetString(1);
            string purpose = reader.GetString(2);
            string description = reader.GetString(3);
            double price = reader.GetDouble(4);
            double coveredArea = reader.GetDouble(5);

            int bedrooms = 0;
            if (!reader.IsDBNull(6))
            {
                bedrooms = reader.GetInt32(6);
            }


            int reception = 0;
            if (!reader.IsDBNull(7))
            {
                reception = reader.GetInt32(7);
            }

            int bathrooms = 0;
            if (!reader.IsDBNull(8))
            {
                bathrooms = reader.GetInt32(8);
            }

            int garage = 0;
            if(!reader.IsDBNull(9))
            { 
                garage = reader.GetInt32(9);
            }

            string features = "";
            if (!reader.IsDBNull(10))
            {
                features = reader.GetString(10);
            }

            
            string image1 = "";
            if (!reader.IsDBNull(11))
            {
                image1 = reader.GetString(11);
            }

            string image2 = "";
            if (!reader.IsDBNull(12))
            {
                image2 = reader.GetString(12);
            }

            string image3 = "";
            if (!reader.IsDBNull(13))
            {
                image3 = reader.GetString(13);
            }

            string image4 = "";
            if (!reader.IsDBNull(14))
            {
                image4 = reader.GetString(14);
            }

            string image5 = "";
            if (!reader.IsDBNull(15))
            {
                image5 = reader.GetString(15);
            }

            string address = "";
            if (!reader.IsDBNull(16))
            {
                address = reader.GetString(16);
            }

            string location = "";
            if (!reader.IsDBNull(17))
            {
                location = reader.GetString(17);
            }

            string previewVideo = "";
            if (!reader.IsDBNull(18))
            {
                previewVideo = reader.GetString(18);
            }


            string propertyType = "";
            if (!reader.IsDBNull(19))
            {
                propertyType = reader.GetString(19);
            }


            int workstation = 0;
            if (!reader.IsDBNull(20))
            {
                workstation = reader.GetInt32(20);
            }
            int pantryKitchen = 0;
            if (!reader.IsDBNull(21))
            {
                pantryKitchen = reader.GetInt32(21);
            }

            int confrenceRoom = 0;
            if (!reader.IsDBNull(22))
            {
                confrenceRoom = reader.GetInt32(22);
            }

            


            int meetingRoom = 0;
            if (!reader.IsDBNull(23))
            {
                meetingRoom = reader.GetInt32(23);
            }


           

            string status = "";
            if (!reader.IsDBNull(24))
            {
                status = reader.GetString(24);
            }

            string category = "";
            if (!reader.IsDBNull(26))
            {
                category = reader.GetString(26);
            }

            string furnishing = "";
            if (!reader.IsDBNull(25))
            {
                furnishing = reader.GetString(25);
            }

           

            double superArea = 0;
            if (!reader.IsDBNull(27))
            {
                superArea = reader.GetDouble(27);
            }
            Property property = new Property(propertyId, title, purpose, description, price, coveredArea, superArea, bedrooms, bathrooms,
                garage, reception, workstation, confrenceRoom, meetingRoom, pantryKitchen, features, image1, image2, image3, image4, image5, address, location, previewVideo, propertyType, status, category, furnishing);
            PropertyList.Add(property);
         
        }
        con.Close();
        return PropertyList;
    }

    public bool DeletePropertyById(int propertyID)
    {
        bool flag = false;
        string query = "delete from property where property_id = " + propertyID;
        MySqlConnection con = new MySqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        MySqlCommand cmd = new MySqlCommand(query, con);
        con.Open();
        if (cmd.ExecuteNonQuery() > 0)
            flag = true;
        con.Close();
        return flag;
    }

}