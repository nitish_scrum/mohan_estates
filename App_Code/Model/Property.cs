﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Property
/// </summary>
public class Property
{
    public int PropertyId { get; set; }
    public string Title { get; set; }
    public string Purpose { get; set; }
    public string Description { get; set; }
    public double Price { get; set; }
    public double CoveredArea { get; set;}
    public double SuperArea { get; set; }
    public int Bedrooms { get; set; }
    public int Bathrooms { get; set; }
    public int Garage { get; set; }
    public int Reception { get; set; }
    public int Workstation { get; set; }
    public int PantryKitchen { get; set; }
    public int ConfrenceRoom { get; set; }
    public int MeetingRoom { get; set; }
    public string Features { get; set; }
    public string Status { get; set; }
    public string Category { get; set; }
    public string Furnishing { get; set; }
    public string Image1 { get; set; }
    public string Image2 { get; set; }
    public string Image3 { get; set; }
    public string Image4 { get; set; }
    public string Image5 { get; set; }
    public string Address { get; set; }
    public string Location { get; set; }
    public string PreviewVideo { get; set; }
    public string PropertyType { get; set; }

    public Property()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public Property(int propertyId,string title,string purpose,string description,double price,double coveredArea,double superArea, int bedrooms,int bathrooms,int garage,int reception,int workstation,int meetingRoom,int confrenceRoom,int pantryKitchen,
                    string features,string image1,string image2,string image3,string image4,string image5,string address,string location,string previewVideo,string propertyType,string status,string category,string furnishing)
    {
        this.PropertyId = propertyId;
        this.Title = title;
        this.Purpose = purpose;
        this.Description = description;
        this.Price = price;
        this.CoveredArea = coveredArea;
        this.SuperArea = superArea;
        this.Bedrooms = bedrooms;
        this.Bathrooms = bathrooms;
        this.Garage = garage;
        this.Reception = reception;
        this.Features = features;
        this.Image1 = image1;
        this.Image2 = image2;
        this.Image3 = image3;
        this.Image4 = image4;
        this.Image5 = image5;
        this.Address = address;
        this.Location = location;
        this.PreviewVideo = previewVideo;
        this.PropertyType = propertyType;
        this.Workstation = workstation;
        this.ConfrenceRoom = confrenceRoom;
        this.MeetingRoom = meetingRoom;
        this.PantryKitchen = pantryKitchen;
        this.Status = status;
        this.Category = category;
        this.Furnishing = furnishing;
    }

    public Property(string title, string purpose, string description, double price, double coveredArea, double superArea, int bedrooms, int bathrooms, int garage, int reception,int workstation,int meetingRoom,int confrenceRoom,int pantryKitchen,
                   string features, string image1, string image2, string image3, string image4, string image5, string address, string location, string previewVideo, string propertyType, string status, string category, string furnishing)
    {
        this.Title = title;
        this.Purpose = purpose;
        this.Description = description;
        this.Price = price;
        this.CoveredArea = coveredArea;
        this.SuperArea = superArea;
        this.Bedrooms = bedrooms;
        this.Bathrooms = bathrooms;
        this.Garage = garage;
        this.Reception = reception;
        this.Features = features;
        this.Image1 = image1;
        this.Image2 = image2;
        this.Image3 = image3;
        this.Image4 = image4;
        this.Image5 = image5;
        this.Address = address;
        this.Location = location;
        this.PreviewVideo = previewVideo;
        this.PropertyType = propertyType;
        this.Workstation = workstation;
        this.ConfrenceRoom = confrenceRoom;
        this.MeetingRoom = meetingRoom;
        this.PantryKitchen = pantryKitchen;
        this.Status = status;
        this.Category = category;
        this.Furnishing = furnishing;
    
    }
}