﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Net;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
    public Utility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void SendEmail(String toAddress, String subject, String body)
    {
        MailMessage mail = new MailMessage("info@applerealestate.uk", toAddress);
        SmtpClient client = new SmtpClient();
        client.Port = 587;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new NetworkCredential("info@applerealestate.uk", "welcome$1");
        client.Host = "webmail.applerealestate.uk";
        mail.IsBodyHtml = true;
        mail.Subject = subject;
        body = "Dear User, <br/> <br/>" + body;
        mail.Body = body;
        client.Send(mail);
    }
    public static void SendEmailWithAttachment(String toAddress, String subject, String body, Attachment attachment)
    {
        MailMessage mail = new MailMessage("info@applerealestate.uk", toAddress);
        SmtpClient client = new SmtpClient();
        client.Port = 587;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        mail.Attachments.Add(attachment);
        client.Credentials = new NetworkCredential("info@applerealestate.uk", "welcome$1");
        client.Host = "webmail.applerealestate.uk";
        mail.Subject = subject;
        mail.Body = body;
        client.Send(mail);
    }
}