﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Commercials : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["admin"]==null)
        {
            Response.Redirect("Login.aspx");
        }
        GetSalesProperty();
    }

  

    protected void GetSalesProperty()
    {
        ArrayList propertyList = null;

        AdminDao dao = new AdminDao();
        propertyList = dao.GetAllPropertyByPurpose("To Sell");
        string txt = "<table class='table'><tr><th>Property ID</th><th>Title</th><th>Price</th><th>Area</th><th>Bedrooms</th><th>Features</th>" +
            "<th>Address</th><th>Details</th></tr>";

        for(int i=0;i<propertyList.Count; i++)
        {
            Property p = (Property)propertyList[i];
            txt += "<tr><td>" + p.PropertyId + "</td>";
            txt+="<td>"+p.Title+"</td>";
            txt += "<td>"+p.Price+"</td>";
            txt += "<td>"+p.CoveredArea+"</td>";
            txt += "<td>" + p.SuperArea + "</td>";
            txt += "<td>"+p.Bedrooms+"</td>";
            txt += "<td>"+p.Features+"</td>";
            txt += "<td>"+p.Address+"</td>";
            txt += "<td><a href = 'PropertyDetails.aspx?propid="+p.PropertyId+"'>Details</a></td></tr>";
        }
        txt += "</table>";
        containertable.InnerHtml = txt;
        
    }
}