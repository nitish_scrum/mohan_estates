﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="admin_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Modern an Admin Panel Category Flat Bootstarp Resposive Website Template | Login :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!----webfonts--->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<!---//webfonts--->  
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="login">
    <div class="login-logo">
    <a><img src="../img/logo.jpg" style="width:200px;" alt=""/></a>
        <hr />
  </div>
  <h2 class="form-heading">login</h2>
  <div class="app-cam">
	  <div>
		<input type="text" class="text" value="E-mail address" runat="server" id="email_txtbox" name="email_txtbox" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'E-mail address';}"/>
		<input type="password" value="Password" runat="server" onfocus="this.value = '';" id="pwd_txtbox" name="pwd_txtbox" onblur="if (this.value == '') {this.value = 'Password';}"/>
		<div class="submit">

        <asp:Button ID="Login" runat="server" Text="Login" OnClick="Login_Click" />
		</div>
		<ul class="new">
			
			<div class="clearfix"></div>
		</ul>
	</div>
  </div>
   
    </div>
    </form>
</body>
</html>
