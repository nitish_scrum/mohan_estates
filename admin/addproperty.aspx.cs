﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_AddProperty : System.Web.UI.Page
{
    static Random r = new Random();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
        {
            Response.Redirect("Login.aspx");
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string title = PropertyTitleTxtBox.Value;
        string purpose = PurposeRadio.SelectedValue;
        string propertyType = TypeRadio.SelectedValue;
        string location = LocationDropdown.SelectedValue;
        string address = AddressTxtBox.Value;
        double price = PriceTxtBox.Value.Trim().Equals("")?0:Convert.ToDouble(PriceTxtBox.Value);
        double coveredArea = CoveredAreaTxtBox.Value.Trim().Equals("")?0:Convert.ToDouble(CoveredAreaTxtBox.Value);
        double superArea = SuperAreaTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToDouble(SuperAreaTxtBox.Value);
        string description = DescriptionTxtArea.Value;
        int bedrooms = BedroomsTxtBox.Value.Trim().Equals("")?0: Convert.ToInt32(BedroomsTxtBox.Value);
        int bathrooms = BathroomsTxtBox.Value.Trim().Equals("")?0: Convert.ToInt32(BathroomsTxtBox.Value);
        int garage = GarageTxtBox.Value.Trim().Equals("")?0: Convert.ToInt32(GarageTxtBox.Value);
        int reception = ReceptionTxtBox.Value.Trim().Equals("")?0: Convert.ToInt32(ReceptionTxtBox.Value);
        int workstation = WorkstationTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(WorkstationTxtBox.Value);
        int pantryKitchen = PantryTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(PantryTxtBox.Value);
        int confrenceRoom = ConfrenceRoomTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(ConfrenceRoomTxtBox.Value);
        int meetingRoom = MeetingRoomTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(MeetingRoomTxtBox.Value);
        string status = StatusTxtBox.Value;
        string category = CategoryTxtBox.Value;
        string furnishing = FurnishingType.SelectedValue;


        string features = "";
        if(CheckBox1.Checked)
        {
            if(features.Equals(""))
            {
                features += CheckBox1.Text;
            }
            else
            {
                features += ", " + CheckBox1.Text;
            }
        }
        if (CheckBox2.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox2.Text;
            }
            else
            {
                features += ", " + CheckBox2.Text;
            }
        }
        if (CheckBox3.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox3.Text;
            }
            else
            {
                features += ", " + CheckBox3.Text;
            }
        }
        if (CheckBox4.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox4.Text;
            }
            else
            {
                features += ", " + CheckBox4.Text;
            }
        }
        if (CheckBox5.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox5.Text;
            }
            else
            {
                features += ", " + CheckBox5.Text;
            }
        }
        if (CheckBox6.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox6.Text;
            }
            else
            {
                features += ", " + CheckBox6.Text;
            }
        }
        if (CheckBox7.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox7.Text;
            }
            else
            {
                features += ", " + CheckBox7.Text;
            }
        }
        if (CheckBox8.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox8.Text;
            }
            else
            {
                features += ", " + CheckBox8.Text;
            }
        }
        if (CheckBox9.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox9.Text;
            }
            else
            {
                features += ", " + CheckBox9.Text;
            }
        }
        if (CheckBox10.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox10.Text;
            }
            else
            {
                features += ", " + CheckBox10.Text;
            }
        }

        if (CheckBox11.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox11.Text;
            }
            else
            {
                features += ", " + CheckBox11.Text;
            }
        }
        if (CheckBox12.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox12.Text;
            }
            else
            {
                features += ", " + CheckBox12.Text;
            }
        }
        if (CheckBox13.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox13.Text;
            }
            else
            {
                features += ", " + CheckBox13.Text;
            }
        }
        if (CheckBox14.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox14.Text;
            }
            else
            {
                features += ", " + CheckBox14.Text;
            }
        }
        if (CheckBox15.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox15.Text;
            }
            else
            {
                features += ", " + CheckBox15.Text;
            }
        }
        if (CheckBox16.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox16.Text;
            }
            else
            {
                features += ", " + CheckBox16.Text;
            }
        }
        if (CheckBox17.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox17.Text;
            }
            else
            {
                features += ", " + CheckBox17.Text;
            }
        }
        if (CheckBox18.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox18.Text;
            }
            else
            {
                features += ", " + CheckBox18.Text;
            }
        }
        string image1 = "";
        string image2 = "";
        string image3 = "";
        string image4 = "";
        string image5 = "";
        string previewVideo = "";
        if (FileUpload1.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload1.FileName);
            image1 = random + "." + ext;
            string imagePath1 = Server.MapPath("~\\images\\property") + "\\" + image1;
            FileUpload1.SaveAs(imagePath1);
        }
        if (FileUpload2.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload2.FileName);
            image2 = random + "." + ext;
            string imagePath2 = Server.MapPath("~\\images\\property") + "\\" + image2;
            FileUpload2.SaveAs(imagePath2);
        }
        if (FileUpload3.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload3.FileName);
            image3 = random + "." + ext;
            string imagePath3 = Server.MapPath("~\\images\\property") + "\\" + image3;
            FileUpload3.SaveAs(imagePath3);
        }


        if (FileUpload4.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload4.FileName);
            image4 = random + "." + ext;
            string imagePath4 = Server.MapPath("~\\images\\property") + "\\" + image4;
            FileUpload3.SaveAs(imagePath4);
        }


        if (FileUpload5.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload5.FileName);
            image5 = random + "." + ext;
            string imagePath5 = Server.MapPath("~\\images\\property") + "\\" + image5;
            FileUpload3.SaveAs(imagePath5);
        }

        Property property = new Property(title, purpose, description, price, coveredArea, superArea, bedrooms, bathrooms, garage, reception,
            workstation, meetingRoom, confrenceRoom, pantryKitchen, features, image1, image2, image3, image4, image5, address,
            location, previewVideo, propertyType, status, category, furnishing);
        AdminDao dao = new AdminDao();
        if(dao.AddNewProperty(property))
        {
            Response.Write("<script>alert('Property added successfully.');window.location.assign('AddProperty.aspx');</script>");
        }
        else
        {
            Response.Write("<script>alert('Something went wrong. Please try again.');</script>");
        }
    }
    private String getFileExtension(String fileName)
    {
        String format = "none";
        int index = fileName.LastIndexOf(".");
        if (index > 0)
        {
            format = fileName.Substring(index + 1);
            format = format.ToLower();
        }
        return format;
    }
}