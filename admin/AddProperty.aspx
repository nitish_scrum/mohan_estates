﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="addproperty.aspx.cs" Inherits="admin_AddProperty" ValidateRequest = "false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <title>Mohans Estates Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<script src="js/jquery.min.js"></script>
     <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
<!----webfonts--->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<!---//webfonts--->  
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="AddProperty.aspx">Mohans Estates Developers</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-nav navbar-right">
				
			    <li >
	        		<a href="Logout.aspx" >Logout</a>
	        		
	      		</li>
			</ul>
			
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <%--<li>
                            <a href="Default.aspx"><i class="fa fa-dashboard fa-fw nav_icon"></i>Dashboard</a>
                        </li>--%>

                        <li>
                            <a href="#"><i class="fa fa-laptop nav_icon"></i>Property<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="AddProperty.aspx">Add Property</a>
                                </li>
                                 
                                 <li>
                                    <a href="Residentials.aspx">Residentials</a>
                                </li>
                                 <li>
                                    <a href="Commercials.aspx">Commercial</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="graphs">
	     <div class="xs">
  	       <h3>Add New Property</h3>
  	         <div class="tab-content">
						<div class="tab-pane active" id="horizontal-form">
							<div class="form-horizontal">
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Property Title</label>
									<div class="col-sm-8">
										<input type="text" class="form-control1" id="PropertyTitleTxtBox" placeholder="Propery Title" runat="server" required="required"/>
									</div>
									<div class="col-sm-2">
										<p class="help-block">
        <asp:Label ID="Label1" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Purpose</label>
									<div class="col-sm-8">
										<asp:RadioButtonList ID="PurposeRadio" runat="server">
                                            
                                            <asp:ListItem Selected="True">Residentials</asp:ListItem>
                                            <asp:ListItem>Commercials</asp:ListItem>
										</asp:RadioButtonList>
									</div>
									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label2" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Type</label>
									<div class="col-sm-8">
										<asp:RadioButtonList ID="TypeRadio" runat="server">
                                            
                                            <asp:ListItem Selected="True">Rent</asp:ListItem>
                                            <asp:ListItem>Sell</asp:ListItem>
										</asp:RadioButtonList>
									</div>
									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label9" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Location</label>
									<div class="col-sm-8">
										<asp:DropDownList class="form-control3" ID="LocationDropdown" runat="server">
                                            <asp:ListItem>Pitampura</asp:ListItem>
                                            <asp:ListItem>Rohini</asp:ListItem>
                                            <asp:ListItem>Netaji Subhas Place</asp:ListItem>
                                            <asp:ListItem>Paschim Vihar</asp:ListItem>
                                           
                                            <asp:ListItem>Yamuna Expressway</asp:ListItem>
                                            <asp:ListItem>Noida</asp:ListItem>
                                            <asp:ListItem>Gurgaon</asp:ListItem>
                                            
										</asp:DropDownList>
									</div>
									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label3" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Address</label>
									<div class="col-sm-8">
										<input type="text" class="form-control1" id="AddressTxtBox" placeholder="Address" runat="server" required="required"/>
									</div>
									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label4" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Price</label>
									<div class="col-sm-8">
										<input type="number" class="form-control1" id="PriceTxtBox" placeholder="Price for the Property" runat="server" required="required"/>
									</div>
									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label5" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Covered Area</label>
									<div class="col-sm-8">
										<input type="number" class="form-control1" id="CoveredAreaTxtBox" placeholder="Area in Square Feets" runat="server" />
									</div>


									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label6" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Super Area</label>
									<div class="col-sm-8">
										<input type="number" class="form-control1" id="SuperAreaTxtBox" placeholder="Area in Square Feets" runat="server" />
									</div>


									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label10" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Status</label>
									<div class="col-sm-8">
										<input type="text" class="form-control1" id="StatusTxtBox" placeholder="Status" runat="server" />
									</div>


									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label11" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Category</label>
									<div class="col-sm-8">
										<input type="text" class="form-control1" id="CategoryTxtBox"  placeholder="Commercial Shop, Office Space etc" runat="server" />
									</div>


									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label12" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Description</label>
									<div class="col-sm-8">
										<textarea id="DescriptionTxtArea" runat="server"></textarea>
									</div>
									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label7" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Furnishing</label>
									<div class="col-sm-8">

                                        <asp:DropDownList ID="FurnishingType" runat="server">
                                            <asp:ListItem>Unfurnished</asp:ListItem>
                                            <asp:ListItem>Semi-furnished</asp:ListItem>
                                            <asp:ListItem>Furnished</asp:ListItem>
                                        </asp:DropDownList>
									</div>


									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label13" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Bedrooms</label>
									<div class="col-sm-2">
										<input type="text" class="form-control1" id="BedroomsTxtBox" placeholder="No. Of Bedrooms" runat="server"/>
									</div>
                                    <label for="focusedinput" class="col-sm-2 control-label">Bathrooms</label>
									<div class="col-sm-2">
										<input type="text" class="form-control1" id="BathroomsTxtBox" placeholder="No. Of Bathrooms" runat="server"/>
									</div>
									
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Workstation</label>
									<div class="col-sm-2">
										<input type="text" class="form-control1" id="WorkstationTxtBox" placeholder="No. Of Cabins" runat="server"/>
									</div>
                                    <label for="focusedinput" class="col-sm-2 control-label">Pantry Kitchen</label>
									<div class="col-sm-2">
										<input type="text" class="form-control1" id="PantryTxtBox" placeholder="No. Of Cabins" runat="server"/>
									</div>
									
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Confrence Room</label>
									<div class="col-sm-2">
										<input type="text" class="form-control1" id="ConfrenceRoomTxtBox" placeholder="No. Of Cabins" runat="server"/>
									</div>
                                    <label for="focusedinput" class="col-sm-2 control-label">Meeting Room</label>
									<div class="col-sm-2">
										<input type="text" class="form-control1" id="MeetingRoomTxtBox" placeholder="No. Of Cabins" runat="server"/>
									</div>
									
								</div>
                               
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Garage</label>
									<div class="col-sm-2">
										<input type="text" class="form-control1" id="GarageTxtBox" placeholder="No. Of Garages" runat="server"/>
									</div>
                                    <label for="focusedinput" class="col-sm-2 control-label">Reception</label>
									<div class="col-sm-2">
										<input type="text" class="form-control1" id="ReceptionTxtBox" placeholder="No. Of Receptions" runat="server"/>
									</div>
									
								</div>
                                <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Features</label>
									<div class="col-sm-8">
                                          <table>
                                            <tr>
                                                <td><asp:CheckBox ID="CheckBox1" runat="server" style="margin-right:20px;" Text="CCTV"/></td>
                                                <td><asp:CheckBox ID="CheckBox2" runat="server" style="margin-right:20px;" Text="24 X 7 Security"/></td>
                                                <td><asp:CheckBox ID="CheckBox3" runat="server" style="margin-right:20px;" Text="Club/Health Center"/><br /></td>
                                            </tr>
                                            <tr>
                                                <td> <asp:CheckBox ID="CheckBox4" runat="server" style="margin-right:20px;"  Text="Gymnasium"/></td>
                                                <td><asp:CheckBox ID="CheckBox5" runat="server" style="margin-right:20px;"  Text="Intercom"/></td>
                                                <td><asp:CheckBox ID="CheckBox6" runat="server"  style="margin-right:20px;" Text="Maintenance Staff"/></td>
                                            </tr>
                                            <tr>
                                                <td><asp:CheckBox ID="CheckBox7" runat="server"  style="margin-right:20px;" Text="Near By School"/></td>
                                                <td><asp:CheckBox ID="CheckBox8" runat="server"  style="margin-right:20px;" Text="Rain Water Harvesting"/></td>
                                                <td><asp:CheckBox ID="CheckBox9" runat="server" style="margin-right:20px;"  Text="Sports Facility"/></td>
                                            </tr>
                                            <tr>
                                                <td><asp:CheckBox ID="CheckBox10" runat="server" style="margin-right:20px;"  Text="Internet"/></td>
                                                <td> <asp:CheckBox ID="CheckBox11" runat="server"  style="margin-right:20px;" Text="Cafeteria"/></td>
                                                <td><asp:CheckBox ID="CheckBox12" runat="server"  style="margin-right:20px;" Text="Convenience Store"/></td>
                                            </tr>
                                            <tr>
                                                <td> <asp:CheckBox ID="CheckBox13" runat="server" style="margin-right:20px;"  Text="Indoor Games"/></td>
                                                <td><asp:CheckBox ID="CheckBox14" runat="server"  style="margin-right:20px;" Text="Landscaped Gardens"/></td>
                                                <td><asp:CheckBox ID="CheckBox15" runat="server"  style="margin-right:20px;" Text="Multipurpose Room"/></td>
                                            </tr>
                                            <tr>
                                                <td> <asp:CheckBox ID="CheckBox16" runat="server" style="margin-right:20px;"  Text="Power Backup"/></td>
                                                <td><asp:CheckBox ID="CheckBox17" runat="server"  style="margin-right:20px;" Text="Shopping Plaza"/></td>
                                                <td><asp:CheckBox ID="CheckBox18" runat="server"  style="margin-right:20px;" Text="Visitors Lounge"/></td>
                                            </tr>
                                        </table>

									</div>
									
								</div>
                                
                                
                               <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Property Image 1</label>
									<div class="col-sm-8">
										<asp:FileUpload ID="FileUpload1" runat="server" required="required"/>
									</div>
									<div class="col-sm-2">
										<p class="help-block"><asp:Label ID="Label8" runat="server" style="display:none;" Text="Label"></asp:Label></p>
									</div>
								</div>
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Property Image 2</label>
									<div class="col-sm-8">
										<asp:FileUpload ID="FileUpload2" runat="server" />
									</div>
									
								</div>
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Property Image 3</label>
									<div class="col-sm-8">
										<asp:FileUpload ID="FileUpload3" runat="server" />
									</div>
									
								</div>
                                 <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Property Image 4</label>
									<div class="col-sm-8">
										<asp:FileUpload ID="FileUpload4" runat="server" />
									</div>
									
								</div>

                                  <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Property Image 5</label>
									<div class="col-sm-8">
										<asp:FileUpload ID="FileUpload5" runat="server" />
									</div>
									
								</div>
  <div class="form-group" style="text-align:center;">
								
									<div class="col-sm-8">
        <asp:Button ID="Button1" runat="server" style="width:50%;" class="btn-success btn" Text="Add Property" OnClick="Button1_Click" />
								</div>
									
								</div>
							</div>
						</div>
					</div>
					
			
 

  </div>
  <div class="copy_layout">
      <p>Copyright © 2016 Mohans Estates. All Rights Reserved | Developed by <a href="http://scrumtechnology.com/" target="_blank">Scrum Technology</a> </p>
  </div>
  </div>
      </div>
      <!-- /#page-wrapper -->
   </div>
    <!-- /#wrapper -->
<!-- Nav CSS -->
<link href="css/custom.css" rel="stylesheet">
<!-- Metis Menu Plugin JavaScript -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
    </form>
</body>
</html>
