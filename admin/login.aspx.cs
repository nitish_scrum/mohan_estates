﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] != null)
        {
            Response.Redirect("AddProperty.aspx");
        }
    }

    protected void Login_Click(object sender, EventArgs e)
    {
        string email = email_txtbox.Value;
        string password = pwd_txtbox.Value;
        AdminDao dao = new AdminDao();
        int flag = dao.AdminLogin(email, password);
        if(flag == 1)
        {
            Session["admin"] = email;
            Response.Redirect("AddProperty.aspx");
        }
        else
        {
            Response.Write("<script>alert('Username or password is incorrect');</script>");
        }
    }
}