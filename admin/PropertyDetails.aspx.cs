﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_AddProperty : System.Web.UI.Page
{
    static Random r = new Random();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        if (!IsPostBack)
        { 
        if (Request.QueryString["propid"]!=null)
        {
            string propertyId = (String)Request.QueryString["propid"];
            FillPropertyDetails(propertyId);
        }
        else
        {
            Response.Redirect("AddProperty.aspx");
        }
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int propertyId = Convert.ToInt32(PropertyIdTxtbox.Value);
        string title = PropertyTitleTxtBox.Value;
        string purpose = PurposeRadio.SelectedValue;
        string propertyType = TypeRadio.SelectedValue;
        string location = LocationDropdown.SelectedValue;
        string address = AddressTxtBox.Value;
        double price = PriceTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToDouble(PriceTxtBox.Value);
        double coveredArea = CoveredAreaTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToDouble(CoveredAreaTxtBox.Value);
        double superArea = SuperAreaTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToDouble(SuperAreaTxtBox.Value);
        string description = DescriptionTxtArea.Value;
        int bedrooms = BedroomsTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(BedroomsTxtBox.Value);
        int bathrooms = BathroomsTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(BathroomsTxtBox.Value);
        int garage = GarageTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(GarageTxtBox.Value);
        int reception = ReceptionTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(ReceptionTxtBox.Value);
        int workstation = WorkstationTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(WorkstationTxtBox.Value);
        int pantryKitchen = PantryTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(PantryTxtBox.Value);
        int confrenceRoom = ConfrenceRoomTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(ConfrenceRoomTxtBox.Value);
        int meetingRoom = MeetingRoomTxtBox.Value.Trim().Equals("") ? 0 : Convert.ToInt32(MeetingRoomTxtBox.Value);
        string status = StatusTxtBox.Value;
        string category = CategoryTxtBox.Value;
        string furnishing = FurnishingType.value;
       

        string features = "";
        if(CheckBox1.Checked)
        {
            if(features.Equals(""))
            {
                features += CheckBox1.Text;
            }
            else
            {
                features += ", " + CheckBox1.Text;
            }
        }
        if (CheckBox2.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox2.Text;
            }
            else
            {
                features += ", " + CheckBox2.Text;
            }
        }
        if (CheckBox3.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox3.Text;
            }
            else
            {
                features += ", " + CheckBox3.Text;
            }
        }
        if (CheckBox4.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox4.Text;
            }
            else
            {
                features += ", " + CheckBox4.Text;
            }
        }
        if (CheckBox5.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox5.Text;
            }
            else
            {
                features += ", " + CheckBox5.Text;
            }
        }
        if (CheckBox6.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox6.Text;
            }
            else
            {
                features += ", " + CheckBox6.Text;
            }
        }
        if (CheckBox7.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox7.Text;
            }
            else
            {
                features += ", " + CheckBox7.Text;
            }
        }
        if (CheckBox8.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox8.Text;
            }
            else
            {
                features += ", " + CheckBox8.Text;
            }
        }
        if (CheckBox9.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox9.Text;
            }
            else
            {
                features += ", " + CheckBox9.Text;
            }
        }
        if (CheckBox10.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox10.Text;
            }
            else
            {
                features += ", " + CheckBox10.Text;
            }
        }

        if (CheckBox11.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox11.Text;
            }
            else
            {
                features += ", " + CheckBox11.Text;
            }
        }
        if (CheckBox12.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox12.Text;
            }
            else
            {
                features += ", " + CheckBox12.Text;
            }
        }
        if (CheckBox13.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox13.Text;
            }
            else
            {
                features += ", " + CheckBox13.Text;
            }
        }
        if (CheckBox14.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox14.Text;
            }
            else
            {
                features += ", " + CheckBox14.Text;
            }
        }
        if (CheckBox15.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox15.Text;
            }
            else
            {
                features += ", " + CheckBox15.Text;
            }
        }
        if (CheckBox16.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox16.Text;
            }
            else
            {
                features += ", " + CheckBox16.Text;
            }
        }
        if (CheckBox17.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox17.Text;
            }
            else
            {
                features += ", " + CheckBox17.Text;
            }
        }
        if (CheckBox18.Checked)
        {
            if (features.Equals(""))
            {
                features += CheckBox18.Text;
            }
            else
            {
                features += ", " + CheckBox18.Text;
            }
        }
        string image1 = "";
        string image2 = "";
        string image3 = "";
        string image4 = "";
        string image5 = "";
        string previewVideo = "";
        if (FileUpload1.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload1.FileName);
            image1 = random + "." + ext;
            string imagePath1 = Server.MapPath("~\\images\\property") + "\\" + image1;
            FileUpload1.SaveAs(imagePath1);
        }
        else
        {
            image1 = Image1Hidden.Value;
        }
        if (FileUpload2.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload2.FileName);
            image2 = random + "." + ext;
            string imagePath2 = Server.MapPath("~\\images\\property") + "\\" + image2;
            FileUpload2.SaveAs(imagePath2);
        }
        else
        {
            image2 = Image2Hidden.Value;
        }
        if (FileUpload3.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload3.FileName);
            image3 = random + "." + ext;
            string imagePath3 = Server.MapPath("~\\images\\property") + "\\" + image3;
            FileUpload3.SaveAs(imagePath3);
        }
        else
        {
            image3 = Image3Hidden.Value;
        }

        if (FileUpload4.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload4.FileName);
            image4 = random + "." + ext;
            string imagePath4 = Server.MapPath("~\\images\\property") + "\\" + image4;
            FileUpload3.SaveAs(imagePath4);
        }
        else
        {
            image4 = Image4Hidden.Value;
        }

        if (FileUpload5.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload5.FileName);
            image5 = random + "." + ext;
            string imagePath5 = Server.MapPath("~\\images\\property") + "\\" + image5;
            FileUpload3.SaveAs(imagePath5);
        }
        else
        {
            image5 = Image5Hidden.Value;
        }
        Property property = new Property(title, purpose, description, price, coveredArea, superArea, bedrooms, bathrooms, garage, reception, workstation, meetingRoom, confrenceRoom, pantryKitchen, features, image1, image2, image3, image4, image5, address, location, previewVideo, propertyType, status, category, furnishing);
        AdminDao dao = new AdminDao();
        if(dao.UpdateProperty(property))
        {
            Response.Write("<script>alert('Property Updated successfully.');window.location.assign('AddProperty.aspx');</script>");
        }
        else
        {
            Response.Write("<script>alert('Something went wrong. Please try again.');</script>");
        }
    }

    protected void FillPropertyDetails(string propertyId)
    {
        Property property = GetPropertyDetailsById(Convert.ToInt32(propertyId));
        PropertyIdTxtbox.Value = property.PropertyId.ToString();
        PropertyTitleTxtBox.Value = property.Title;
        PurposeRadio.SelectedValue = property.Purpose;
        TypeRadio.SelectedValue = property.PropertyType;
        LocationDropdown.SelectedValue = property.Location;

        AddressTxtBox.Value = property.Address;
        PriceTxtBox.Value = property.Price.ToString();
        AreaTxtBox.Value = property.Area.ToString();
        DescriptionTxtArea.Value = property.Description;
        BedroomsTxtBox.Value = property.Bedrooms.ToString();
        BathroomsTxtBox.Value = property.Bathrooms.ToString();
        GarageTxtBox.Value = property.Garage.ToString();
        ReceptionTxtBox.Value = property.Reception.ToString();

        string features = property.Features;
        if (features.Contains("CCTV"))
        {
            CheckBox1.Checked = true;
        }
        if (features.Contains("24 X 7 Security"))
        {
            CheckBox2.Checked = true;
        }
        if (features.Contains("Club/Health Center"))
        {
            CheckBox3.Checked = true;
        }
        if (features.Contains("Gymnasium"))
        {
            CheckBox4.Checked = true;
        }
        if (features.Contains("Intercom"))
        {
            CheckBox5.Checked = true;
        }
        if (features.Contains("Maintenance Staff"))
        {
            CheckBox6.Checked = true;
        }
        if (features.Contains("Near By School"))
        {
            CheckBox7.Checked = true;
        }
        if (features.Contains("Rain Water Harvesting"))
        {
            CheckBox8.Checked = true;
        }
        if (features.Contains("Sports Facility"))
        {
            CheckBox9.Checked = true;
        }
        if (features.Contains("Internet"))
        {
            CheckBox10.Checked = true;
        }
        if (features.Contains("Cafeteria"))
        {
            CheckBox11.Checked = true;
        }
        if (features.Contains("Convenience Store"))
        {
            CheckBox12.Checked = true;
        }
        if (features.Contains("Indoor Games"))
        {
            CheckBox13.Checked = true;
        }
        if (features.Contains("Landscaped Gardens"))
        {
            CheckBox14.Checked = true;
        }
        if (features.Contains("Multipurpose Room"))
        {
            CheckBox15.Checked = true;
        }
        if (features.Contains("Power Backup"))
        {
            CheckBox16.Checked = true;
        }
        if (features.Contains("Shopping Plaza"))
        {
            CheckBox17.Checked = true;
        }
        if (features.Contains("Visitors Lounge"))
        {
            CheckBox18.Checked = true;
        }
        string displayImage1 = property.Image1;
        string displayImage2 = property.Image2;
        string displayImage3 = property.Image3;
        string displayImage4 = property.Image4;
        string displayImage5 = property.Image5;
        Image1Hidden.Value = displayImage1;
        Image2Hidden.Value = displayImage2;
        Image3Hidden.Value = displayImage3;
        Image4Hidden.Value = displayImage4;
        Image5Hidden.Value = displayImage5;
        Image1.ImageUrl = displayImage1.Equals("") ? displayImage1 : "../images/property/" + displayImage1;
        Image2.ImageUrl = displayImage2.Equals("") ? displayImage2 : "../images/property/" + displayImage2;
        Image3.ImageUrl = displayImage2.Equals("") ? displayImage3 : "../images/property/" + displayImage3;
        Image4.ImageUrl = displayImage2.Equals("") ? displayImage4 : "../images/property/" + displayImage4;
        Image5.ImageUrl = displayImage2.Equals("") ? displayImage5 : "../images/property/" + displayImage5;

        string image1 = "";
        string image2 = "";
        string image3 = "";
        string image4 = "";
        string image5 = "";
        string previewVideo = "";
        if (FileUpload1.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload1.FileName);
            image1 = random + "." + ext;
            string imagePath1 = Server.MapPath("~\\images\\property") + "\\" + image1;
            FileUpload1.SaveAs(imagePath1);
        }
        if (FileUpload2.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload2.FileName);
            image2 = random + "." + ext;
            string imagePath2 = Server.MapPath("~\\images\\property") + "\\" + image2;
            FileUpload2.SaveAs(imagePath2);
        }
        if (FileUpload3.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload3.FileName);
            image3 = random + "." + ext;
            string imagePath3 = Server.MapPath("~\\images\\property") + "\\" + image3;
            FileUpload3.SaveAs(imagePath3);
        }


        if (FileUpload4.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload4.FileName);
            image4 = random + "." + ext;
            string imagePath4 = Server.MapPath("~\\images\\property") + "\\" + image4;
            FileUpload3.SaveAs(imagePath4);
        }


        if (FileUpload5.HasFile)
        {
            int random = r.Next();
            string ext = getFileExtension(FileUpload5.FileName);
            image5 = random + "." + ext;
            string imagePath5 = Server.MapPath("~\\images\\property") + "\\" + image5;
            FileUpload3.SaveAs(imagePath5);
        }
    }

    protected Property GetPropertyDetailsById(int propertyId)
    {
        Property property = null;
        AdminDao dao = new AdminDao();
        property = dao.GetPropertyDetailsById(propertyId);
        return property;
    }
    private String getFileExtension(String fileName)
    {
        String format = "none";
        int index = fileName.LastIndexOf(".");
        if (index > 0)
        {
            format = fileName.Substring(index + 1);
            format = format.ToLower();
        }
        return format;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        AdminDao dao = new AdminDao();
        String propertyId = PropertyIdTxtbox.Value;
       
        String purpose = PurposeRadio.SelectedValue;
        bool flag = dao.DeletePropertyById(Int32.Parse(propertyId));
        String redirectPage = "";
        if (purpose.Equals("To Sell"))
            redirectPage = "Sales.aspx";
        else if (purpose.Equals("Commercials"))
            redirectPage = "Commercials.aspx";
        else if (purpose.Equals("Residentials"))
            redirectPage = "Residentials.aspx";
        if (flag)
        {
            
            Response.Write("<script>alert('Deleted Successfully');window.location.assign('" + redirectPage + "');</script>");
            
        }
    }
}