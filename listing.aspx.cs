﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class listing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadProperties();
    }
    protected void LoadProperties()
    {
        String searchQuery = "";
        if (Session["SearchQuery"] != null)
        {
            searchQuery = (String)Session["SearchQuery"];
        }
        else
        {
            searchQuery = "select * from property";
        }
        AdminDao dao = new AdminDao();

        ArrayList propertyList = dao.SearchProperty(searchQuery);
        string propertyTxt = "";


        for (int i = 1; i <= propertyList.Count; i++)
        {
            Property property = (Property)propertyList[i - 1];
            propertyTxt += "<div class='col-md-4 listing-single-item'><div class='item-inner'><div class='image-wrapper'>" +
            "<a href='property.aspx?prop="+property.PropertyId+"'><img src='images/property/" + property.Image1 + "' alt='gallery'></a><a href='property.aspx?prop=" + property.PropertyId + "' class='fa ";
            if (property.Purpose.Equals("Residential"))
            {
                propertyTxt += "fa-home";
            }
            else
            {
                propertyTxt += "fa-university";
            }
            propertyTxt += " property-type-icon'></a>" +
            "<a href='#' class='featured'><i class='fa fa-star'></i>featured</a></div>" +
            "<div class='desc-box'><h4><a href='property.aspx?prop="+property.PropertyId+"'>" + property.Address + "</a></h4>" +
            "<ul class='slide-item-features item-features'><li><span class='fa fa-arrows-alt'></span>" + property.CoveredArea + "</li></ul>" +
            "<ul class='slide-item-features item-features'><li><span class='fa fa-arrows-alt'></span>" + property.SuperArea + "</li></ul>" +
           
            "<div class='buttons-wrapper'><a href='property.aspx?prop=" + property.PropertyId + "' class='yellow-btn'>₹ " + property.Price + "</a>" +
            "<a href='property.aspx?prop=" + property.PropertyId + "' class='gray-btn'><span class='fa fa-file-text-o'></span>Details</a></div>" +
            "<div class='clearfix'></div></div></div></div>";
        }
        PropertyContainer.InnerHtml = propertyTxt;

        Session["SearchQuery"] = null;
    }

    protected ArrayList GetCommercialsProperty()
    {
        ArrayList propertyList = null;

        AdminDao dao = new AdminDao();
        propertyList = dao.GetAllPropertyByPurpose("Commercials");

        return propertyList;
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        string purpose = PurposeDropdown.SelectedValue;
        string location = LocationDropdown.SelectedValue;
        string propertyType = PropertyTypeDropdown.SelectedValue;

        string minPrice = MinPriceDropdown.SelectedValue;
        string maxPrice = MaxPriceDropdown.SelectedValue;

        String query = "select * from property where property_type = '" + propertyType + "'";

        if (!purpose.Equals("All"))
        {
            query += " and purpose = '" + purpose + "'";
        }
        if (!location.Equals("All"))
        {
            query += " and location = '" + location + "'";
        }

        if (!minPrice.Equals("0"))
        {
            query += " and price > " + minPrice;
        }
        if (!maxPrice.Equals("0"))
        {
            query += " and price < " + maxPrice;
        }

        Session["SearchQuery"] = query;
        Response.Redirect("listing.aspx");
    }
}