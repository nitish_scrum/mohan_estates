﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EnquirySlider.ascx.cs" Inherits="controls_EnquirySlider" %>

  <style>

	.form-parent {
    width: 323px;
    height: auto;
    background: transparent url(img/contact-button.png) no-repeat left 0px;
    position: fixed;
    top: 130px;
    right: -280px;
    z-index: 9999999;
    transition: all ease .6s;
    -moz-transition: all ease .6s;
    -webkit-transition: all ease .6s;
    -o-transition: all ease .6s;
}

.form-parent:hover {right:0;}

.cc-float-form {
    background: rgba(0,0,0,0.5);
    color: #fafafa;
    padding: 10px;
    width: 275px;
    border-radius: 0 0px 5px 0;
	margin-left:50px;
}

.contact-form-area {
    background: #fff;
    padding: 7px 5px;
    border: none;
    font-family: Verdana, Geneva, sans-serif;
    color: #000;
    font-size: 12px;
    width: 98%;
}
.img-responsive{
    max-width:100%;
	height:auto;
	box-sizing:border-box;
}

.agree{
background: none repeat scroll 0 0 #1e1e1e;
    border: 2px solid #fff;
    border-radius: 50px;
    color: rgb(255, 255, 255);
    font-weight: bold;
    padding: 4px 11px;
    /* position: absolute; */
    margin-left: 60px;
    margin-top:5px;
    top: -12px;
    z-index: 9999;
    height: 36px;
    opacity: 1;
    text-shadow: none;
	float:left;
}

  </style>
<script>
    //function OpenEnquiry() {

    //    $(".form-parent").css("right", "0px");
    //}

    //function closeEnquiry() {
    //    $(".form-parent").css("right", "-280px");
    //}
</script>

<div class="form-parent hidden-xs">
  
<div name="contact-form" class="cc-float-form" id="floatContact" onsubmit="return floatsubmitContact()">
<br />
Name:<span style="color:red;">*</span><br>
<input class="contact-form-area" id="ContactForm1_contact-form-name" name="name" size="30" value="" type="text" required="">

Mobile:
<span style="color:red;">*</span><br>
<input class="contact-form-area" id="ContactForm1_contact-form-email" maxlength="10" onkeypress="return isNumber(event)" name="mobile" size="30" value="" type="text" required="">

Email:
<span style="color:red;">*</span><br>
<input class="contact-form-area" id="ContactForm1_contact-form-email" name="email" size="30" value="" type="email" required="">

Subject:<br>
<input class="contact-form-area" id="ContactForm1_contact-form-name" name="subject" size="30" value="" type="text">

Message: <span style="color:red;">*</span><br>
<textarea class="contact-form-area" id="ContactForm1_contact-form-email-message" name="msg" cols="28" rows="3" required=""></textarea>

<div style="text-align: center; max-width: 222px; width: 100%">
<p class="contact-form-error-message" style="color:#fff;" id="floatContactForm1"> </p>
</div>
<button class="btn btn-danger pull-right">Submit</button>
<p></p>
<div class="clearfix"></div>
</div>
</div>