﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="controls_Footer" %>

	<footer id="footer-section">
    	<a href="#header-section" class="scroll-to" id="to-top"></a>
    	<div class="container footer-info">
        	<div class="row">
            	<div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-6 footer-column onscroll-animate" style="text-align:justify;">
                            <h4><%--<img alt="Mohans Estates" src="images/logo3.png" style="height:100px;">--%>Mohans Estatess
  </h4>
                            <p>Apple Estate agents is an independent, family run estate agent that aims to provide the best possible customer service in relation to sales, lettings and property management.  We have on a personal level recognised the strain and stress regarding moving houses /property management and therefore our role is to provide professional advice, honest evaluations and regular communication throughout.</p>
                            <a href="About.aspx" class="read-more-link-alt">Read more</a>
                        </div>
                        <div class="col-sm-6 footer-column onscroll-animate" data-delay="400">
                            <h4>Our Services</h4>
                            <ul class="list-links-simple">
                                <li><a href="PropertyValuation.aspx">Property Valuation</a></li>
                                <li><a href="MovingHome.aspx">Moving Home</a></li>
                                <li><a href="Mortgages.aspx">Mortgages</a></li>
                                <li><a href="Commercials.aspx">Commercial Properties</a></li>
                            </ul>
                        </div>
                    </div><!-- .row -->
                </div><!-- .col-md-6 -->
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-6 footer-column font-normal onscroll-animate" data-delay="600">
                            <h4>Contact Info</h4>
                            <div class="icon-opening-wrapper">
                                <div class="icon-opening-container">
                                    <div class="icon-opening"><i class="fa fa-phone"></i></div>
                                    <div class="icon-opening-content">+44 7957 601453</div>
                                </div>
                            </div>
                            
                            <div class="icon-opening-wrapper">
                                <div class="icon-opening-container">
                                    <div class="icon-opening"><i class="fa fa-globe text-big"></i></div>
                                    <div class="icon-opening-content">66A Stainforth RD,
                                                            <br />Ilford Essex.<br />
IG2 7EJ
</div>
                                </div>
                            </div>
                            <div class="icon-opening-wrapper">
                                <div class="icon-opening-container">
                                    <div class="icon-opening"><i class="fa fa-envelope-alt"></i></div>
                                    <div class="icon-opening-content">
                                     <a href="mailto:info@applerealestate.uk">info@applerealestate.uk</a><br/>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 footer-column onscroll-animate" data-delay="800">
                            <h4>Newsletter</h4>
                            <p>Subscribe for out newsletter, and we will keep you inform of new offers.</p>
                            <div class="form-subscribe" id="rss-subscribe" method="post" data-email-not-set-msg="Email must be set" data-ajax-fail-msg="Ajax could not set the request" data-success-msg="Email successfully added">
                                <input type="text" name="email" placeholder="your email...">
                                <div class="text-right">
                                	<input type="submit" value="Submit">
                                </div>
                                <p class="return-msg"></p>
                            </div>
                        </div>
                    </div><!-- .row -->
                </div><!-- .col-md-6 -->
            </div><!-- .row -->
        </div><!-- .container -->
        <div class="main-menu-alt">
        	<div class="container">
                <div class="menu-button"><i class="fa fa-reorder"></i></div>
                <nav id="bottom-menu" class="menu-container menu-container-slide">
                	<div class="underscore-container">
                    <ul>
                        <li><a href="Default.aspx">Home</a></li>
                        <li><a href="Sales.aspx">Sale</a></li>
                        <li><a href="Residentials.aspx">Lettings</a></li>
                        <li><a href="Commercials.aspx">Commercials</a></li>
                        <li><a href="About.aspx">About Us</a></li>
                        <li><a href="Testimonial.aspx">Testimonial</a></li>
                        <li><a href="Contact.aspx">Contact Us</a></li>
                    </ul>
                    
                	</div>
                </nav>
            </div><!-- .container -->
      	</div><!-- .main-menu-alt -->
        <div class="site-info">
        	<div class="container">
                <div class="row">
                    <div class="col-xs-6 onscroll-animate" data-animation="fadeInLeft">
                        <p>2016 All rights Reserved. Made with <i class="red fa fa-heart-empty"></i> by <a href="http://scrumtechnology.com/"><em>Scrum Technology</em></a></p>
                    </div>
                    <div class="col-xs-6 text-right onscroll-animate" data-animation="fadeInRight">
                    	<div class="socials-wrapper">
                            <div class="social-round-container">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </div>
                            <div class="social-round-container">
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </div>
                            <div class="social-round-container">
                                <a href="#"><i class="fa fa-rss"></i></a>
                            </div>
                            <div class="social-round-container">
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                            <div class="social-round-container">
                                <a href="#"><i class="fa fa-tumblr"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .site-info -->
    </footer>