﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register Src="~/controls/EnquirySlider.ascx" TagName="slider" TagPrefix="mohans" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <!--Page Title-->
<title>Mohan Estates Developers</title>
<!--Meta Tags-->
<meta charset="UTF-8">
<meta name="author" content="">
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<!-- Set Viewport-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css"/>
<link rel="stylesheet" href="css/font-awesome.min.css"/>
<link rel="stylesheet" href="css/flexslider.css"/>
<link rel="stylesheet" href="css/select-theme-default.css"/>
<link rel="stylesheet" href="css/owl.carousel.css"/>
<link rel="stylesheet" href="css/owl.theme.css"/>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<style type="text/css">
<!--
.style1 {font-size: 18px}
-->
</style>
<%-- <script>
        var myIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("slides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            myIndex++;
            if (myIndex > x.length) { myIndex = 1 }
            x[myIndex - 1].style.display = "block";
            setTimeout(carousel, 1000); // Change image every 2 seconds
        }
</script>--%>
</head>
<body>
    <form id="form1" runat="server">
        <mohans:slider ID="enquirySlider" runat="server" />
    <div>
    <header>
<div id="top-strip">
	<div class="container">
		<ul class="pull-left social-icons">
			<li><a href="#" class="fa fa-google-plus"></a></li>
			<li><a href="#" class="fa fa-twitter"></a></li>
			<li><a href="#" class="fa fa-pinterest"></a></li>
			<li><a href="#" class="fa fa-dribbble"></a></li>
			<li><a href="#" class="fa fa-linkedin"></a></li>
			<li><a href="#" class="fa fa-facebook"></a></li>
		</ul>
		<div id="login-box" class='pull-right'>
			<a class='fa fa-phone'><span>+91-9818060989 | +91-9999654898</span></a>		</div>
	</div>
</div>
</header>
<!-- /Header -->
<div class="slider-section">
	<div id="premium-bar">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="Default.aspx"><img src="img/logo.jpg" alt="logo"></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="Default.aspx">Home</a></li>
						<li><a href="About.aspx">About Us</a></li>
						<li><a href="listing.aspx">Properties</a></li>
						<li><a href="interiors.aspx">Interiors</a></li>
                        <li><a href="PostRequirement.aspx">Post Requirement</a></li>
						
						<li><a href="Contactus.aspx">Contact Us</a></li>
						
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
			</nav>
		</div>
	</div>
	<!-- Slider-Section -->
	<div class="main-flexslider">
		<ul class="slides">
			<li class='slides' id='slide-n1'><img src="img/banner-ist.jpg" alt="slide 01" />
                <div class="slide-box">
                    <h2>Welcome to Mohans Estates Developers</h2>
                  
                </div>
			</li>

			<li class='slides' id='slide-n2'><img src="img/jpeg-3.jpg" alt="slide" />
			 <div class="slide-box">
                    <h2>Scaling Heights in every Endeavours</h2>
                  
                </div>
			</li>
			<li class='slides' id='slide-n3'><img src="img/bannerhome.jpg" alt="slide" />
			 <div class="slide-box">
                    <h2>Welcome to Mohan Estates Developers</h2>
                  
                </div>
			</li>
		</ul>
	</div>
</div>
<!-- Search-Section -->
<div class="search-section">
	<div class="container">
		
			<div class="select-wrapper select-big" id='select-rent'>
				<p>
					 Looking For
				</p>
                <asp:DropDownList ID="PropertyTypeDropdown" runat="server" class='elselect'>
                    <asp:ListItem>Rent</asp:ListItem>
                    <asp:ListItem>Sale</asp:ListItem>
                   
                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big" id='select-property'>
				<p>
					 Property type
				</p>
                <asp:DropDownList ID="PurposeDropdown" runat="server" class='elselect'>
                    <asp:ListItem>Residential</asp:ListItem>
                    <asp:ListItem>Commercials</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big">
				<p>
					 Location
				</p>
                  <asp:DropDownList ID="LocationDropdown" runat="server" class='elselect'>
                    <asp:ListItem>All</asp:ListItem>
                       <asp:ListItem>Pitampura</asp:ListItem>
                                            <asp:ListItem>Rohini</asp:ListItem>
                                            <asp:ListItem>Netaji Subhas Place</asp:ListItem>
                                            <asp:ListItem>Paschim Vihar</asp:ListItem>
                                           
                                            <asp:ListItem>Yamuna Expressway</asp:ListItem>
                                            <asp:ListItem>Noida</asp:ListItem>
                                            <asp:ListItem>Gurgaon</asp:ListItem>
                    
                </asp:DropDownList>
				
			</div>
			
			
			
			<div class="select-wrapper select-big">
				<p>
					 Min Price
				</p>
                 <asp:DropDownList ID="MinPriceDropdown" runat="server" class='elselect'>
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="10000">₹ 10000</asp:ListItem>
                      <asp:ListItem Value="25000">₹ 25000</asp:ListItem>
                      <asp:ListItem Value="50000">₹ 50000</asp:ListItem>
                      <asp:ListItem Value="100000">₹ 100000</asp:ListItem>
                      <asp:ListItem Value="125000">₹ 125000</asp:ListItem>
                      <asp:ListItem Value="150000">₹ 150000</asp:ListItem>

                </asp:DropDownList>
				
			</div>
			<div class="select-wrapper select-big">
				<p>
					 Max Price
				</p>
				<asp:DropDownList ID="MaxPriceDropdown" runat="server" class='elselect'>
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="100000">₹ 100000</asp:ListItem>
                      <asp:ListItem Value="250000">₹ 250000</asp:ListItem>
                      <asp:ListItem Value="500000">₹ 500000</asp:ListItem>
                      <asp:ListItem Value="1000000">₹ 1000000</asp:ListItem>
                      <asp:ListItem Value="1250000">₹ 1250000</asp:ListItem>
                      <asp:ListItem Value="1500000">₹ 1500000</asp:ListItem>

                </asp:DropDownList>
			</div>
            <asp:Button ID="SearchButton" runat="server" Text="Search" class='yellow-btn' OnClick="SearchButton_Click" />
			
	
	</div>
</div>
<!-- Recent-Listings-Section -->
<div class="recent-listings">
	<div class="container container1">
		<div class="title-box">
			<h3>Featured Properties</h3>
			<div class="bordered">
			</div>
		</div>
		<div class="row listings-items-wrapper" id="featured_property" runat="server">
			<%--<div class="col-md-4 listing-single-item">
				<div class="item-inner">
					<div class="image-wrapper">
						<img src="img/blue2.jpg" alt="gallery">
						<a href="#" class='fa fa-home property-type-icon'></a>
						<a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
					</div>
					<div class="desc-box">
						<h4><a href="#">Plot No. A 1 Netaji Subhash Place, Ring Road, Pitampura</a></h4>
						<ul class="slide-item-features item-features">
							<li><span class="fa fa-arrows-alt"></span>5000 Sq Ft</li>
							
						</ul>
						<div class="buttons-wrapper">
							<a href="#" class="yellow-btn">₹ 3,70,000</a>
							<a href="#" class="gray-btn"><span class="fa fa-file-text-o"></span>Details</a>
						</div>
						<div class="clearfix">
						</div>
					</div>
				</div>
			</div>
			<!-- /Single-item -->
            <div class="col-md-4 listing-single-item">
                <div class="item-inner">
                    <div class="image-wrapper">
                        <img src="img/blue2.jpg" alt="gallery">
                        <a href="#" class='fa fa-home property-type-icon'></a>
                        <a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
                    </div>
                    <div class="desc-box">
                        <h4><a href="#">Plot No. A 1 Netaji Subhash Place, Ring Road, Pitampura</a></h4>
                        <ul class="slide-item-features item-features">
                            <li><span class="fa fa-arrows-alt"></span>5000 Sq Ft</li>

                        </ul>
                        <div class="buttons-wrapper">
                            <a href="#" class="yellow-btn">₹ 3,70,000</a>
                            <a href="#" class="gray-btn"><span class="fa fa-file-text-o"></span>Details</a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
			<!-- /Single-item -->
            <div class="col-md-4 listing-single-item">
                <div class="item-inner">
                    <div class="image-wrapper">
                        <img src="img/blue2.jpg" alt="gallery">
                        <a href="#" class='fa fa-home property-type-icon'></a>
                        <a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
                    </div>
                    <div class="desc-box">
                        <h4><a href="#">Plot No. A 1 Netaji Subhash Place, Ring Road, Pitampura</a></h4>
                        <ul class="slide-item-features item-features">
                            <li><span class="fa fa-arrows-alt"></span>5000 Sq Ft</li>

                        </ul>
                        <div class="buttons-wrapper">
                            <a href="#" class="yellow-btn">₹ 3,70,000</a>
                            <a href="#" class="gray-btn"><span class="fa fa-file-text-o"></span>Details</a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
			<!-- /Single-item -->
            <div class="col-md-4 listing-single-item">
                <div class="item-inner">
                    <div class="image-wrapper">
                        <img src="img/blue2.jpg" alt="gallery">
                        <a href="#" class='fa fa-home property-type-icon'></a>
                        <a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
                    </div>
                    <div class="desc-box">
                        <h4><a href="#">Plot No. A 1 Netaji Subhash Place, Ring Road, Pitampura</a></h4>
                        <ul class="slide-item-features item-features">
                            <li><span class="fa fa-arrows-alt"></span>5000 Sq Ft</li>

                        </ul>
                        <div class="buttons-wrapper">
                            <a href="#" class="yellow-btn">₹ 3,70,000</a>
                            <a href="#" class="gray-btn"><span class="fa fa-file-text-o"></span>Details</a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
			<!-- /Single-item -->
            <div class="col-md-4 listing-single-item">
                <div class="item-inner">
                    <div class="image-wrapper">
                        <img src="img/blue2.jpg" alt="gallery">
                        <a href="#" class='fa fa-home property-type-icon'></a>
                        <a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
                    </div>
                    <div class="desc-box">
                        <h4><a href="#">Plot No. A 1 Netaji Subhash Place, Ring Road, Pitampura</a></h4>
                        <ul class="slide-item-features item-features">
                            <li><span class="fa fa-arrows-alt"></span>5000 Sq Ft</li>

                        </ul>
                        <div class="buttons-wrapper">
                            <a href="#" class="yellow-btn">₹ 3,70,000</a>
                            <a href="#" class="gray-btn"><span class="fa fa-file-text-o"></span>Details</a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
			<!-- /Single-item -->
            <div class="col-md-4 listing-single-item">
                <div class="item-inner">
                    <div class="image-wrapper">
                        <img src="img/blue2.jpg" alt="gallery">
                        <a href="#" class='fa fa-home property-type-icon'></a>
                        <a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
                    </div>
                    <div class="desc-box">
                        <h4><a href="#">Plot No. A 1 Netaji Subhash Place, Ring Road, Pitampura</a></h4>
                        <ul class="slide-item-features item-features">
                            <li><span class="fa fa-arrows-alt"></span>5000 Sq Ft</li>

                        </ul>
                        <div class="buttons-wrapper">
                            <a href="#" class="yellow-btn">₹ 3,70,000</a>
                            <a href="#" class="gray-btn"><span class="fa fa-file-text-o"></span>Details</a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
			<!-- /Single-item -->--%>
		</div>
	</div>
</div>
<!-- Agents-Section -->
<div class="agents-section">
	<div class="container container1">
		
		<div class="row">
			<div class="col-md-4 single-agent">
				
				<div class="desc-box">
					<h4>WHY CHOOSE US</h4>
					<p class="person-number">
                        For exceptional service choosing Mohans Estates Develoeprs is the right choice!  We pride ourselves on being Professional, Friendly, Creative and Respectful.  We understand that – whether you are buying or rennting property – a Realtor’s quality of service is paramount in delivering a positive and hassle-free experience.  
					</p>
				
				</div>
			</div>
            <div class="col-md-4 single-agent">

                <div class="desc-box">
                    <h4>WHY INTERIORS</h4>
                    <p class="person-number">
                        When it comes to Interior, we can help you the best. We have specialized interior decorators and designers to make your office and home look awesome. We do nothing but awesomeness. Cutting edge solution for all your needs.
                    </p>

                </div>
            </div>
            <div class="col-md-4 single-agent">

                <div class="desc-box">
                    <h4>WHO WE ARE</h4>
                    <p class="person-number">
                        MOHANS ESTATES DEVELOPERS has been a pioneer in the fields of Real Estate Development. In real estate our expertise lies in the development and promotion of Shopping Malls, Commercial Buildings, Commercial Office Spaces and Shop, Residential Floor and Plots.

                    </p>

                </div>
            </div>
          
           
           
		</div>
	</div>
</div>
<!-- Services-Section -->

<!-- footer-section -->
<footer>
<div class="container">
	<div class="col-md-3 footer-about">
                <a class="logo" href="#"><img src="img/footer-logo.jpg" alt="logo"></a>
                <p>
                     MOHANS ESTATES DEVELOPERS has been a pioneer in the fields of Real Estate Development. In real estate our expertise lies
in the development and promotion of Farmhouses, Residential Plots /Buildings, Shopping Malls and CommercialBuildings.
                </p>
            </div>
            <div class="col-md-3 footer-recent-posts">
                <h3 class='footer-title'>ADDRESS</h3>
                <ul>
                    <li>
                        <a href="#">
                            <i class="fa fa-map-marker"></i>  Ground Floor-6, Dmall,
                            Netaji subhash palace, New Delhi
                        </a>
                    </li>
                    <li><a href="#"><i class="fa fa-phone"></i>+91-98 1806 0989</a></li>
                    <li><a href="#"><i class="fa fa-globe"></i>mohansestates.com</a></li>
                    <li><a href="#"><i class="fa fa-globe"></i>dmall.in.net</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>contact@mohansestates.com</a></li>
                </ul>
            </div>
	<div class="col-md-3 footer-contact-info">
		<h3 class='footer-title'>WE ARE SOCIAL</h3>
		<p class="website-number">
			<i class="fa fa-facebook"></i> Facebook
		</p>
		<p class="website-email">
			<i class="fa fa-twitter"></i> Twitter
		</p>
		<p class="website-fax">
			<i class="fa fa-google-plus"></i>Google Plus
		</p>
	</div>
    <div class="col-md-3 footer-recent-posts">
        <h3 class='footer-title'>Other Links</h3>
        <ul>
            <li>
                <a href="About.aspx">About Us</a>
            </li>
            <li>
                <a href="Contactus.aspx">Contact Us</a>
            </li>
            
            <li>
                <a>Career</a>
            </li>
            <li>
                <a href="TermsCondition.aspx">Terms and Conditions</a>
            </li>
            <li>
                <a href="PrivacyPolicy.aspx">Privacy Policies</a>
            </li>
        </ul>
    </div>
</div>
</footer>
<div class="bottom-strip">
	<div class="container">
		<div class="col-md-4">
			<p class='pull-left'>
				 © 2017 Mohan Estates Developers, All Rights Reserved
			</p>
		</div>
		<div class="col-md-4 bottom-strip-middle">
			<a href="#top" class='fa fa-arrow-circle-up' id='backtop-btn'></a>
		</div>
		<div class="col-md-4">
			<ul class="social-icons">
				<li><a href="#" class="fa fa-google-plus"></a></li>
				<li><a href="#" class="fa fa-twitter"></a></li>
				<li><a href="#" class="fa fa-pinterest"></a></li>
				<li><a href="#" class="fa fa-dribbble"></a></li>
				<li><a href="#" class="fa fa-linkedin"></a></li>
				<li><a href="#" class="fa fa-facebook"></a></li>
			</ul>
		</div>
	</div>
</div>
<!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/select.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
    </div>
    </form>
</body>
</html>
